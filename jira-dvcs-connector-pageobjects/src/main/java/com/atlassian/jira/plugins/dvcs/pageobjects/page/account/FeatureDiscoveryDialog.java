package com.atlassian.jira.plugins.dvcs.pageobjects.page.account;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.AbstractComponentPageObject;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Represents the 'Feature Discovery' part of the page after approving a connection
 */
public class FeatureDiscoveryDialog extends AbstractComponentPageObject {

    public static final String FEATURE_DISCOVERY_DIALOG_ID = "dvcs-feature-discovery-dialog";

    @ElementBy(className = "aui-iconfont-close-dialog")
    private PageElement closeDialogLink;

    public FeatureDiscoveryDialog(PageElement container) {
        super(container);
    }

    public PageElement getCloseDialogLink() {
        return closeDialogLink;
    }
}
