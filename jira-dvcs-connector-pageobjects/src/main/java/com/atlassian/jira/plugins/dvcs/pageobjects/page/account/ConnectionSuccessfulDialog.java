package com.atlassian.jira.plugins.dvcs.pageobjects.page.account;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.AbstractComponentPageObject;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Represents the 'Connection Successful' part of the page after approving a connection
 */
public class ConnectionSuccessfulDialog extends AbstractComponentPageObject {

    public static final String CONNECTION_SUCCESSFUL_DIALOG_ID = "connection-successful-dialog";

    @ElementBy(id = "autolink-checkbox")
    private PageElement autoLinkRepositoryCheckbox;

    @ElementBy(id = "dialog-submit-button")
    private PageElement submitButton;

    public ConnectionSuccessfulDialog(PageElement container) {
        super(container);
    }

    public PageElement getAutoLinkRepositoryCheckbox() {
        return autoLinkRepositoryCheckbox;
    }

    public PageElement getSubmitButton() {
        return submitButton;
    }
}
