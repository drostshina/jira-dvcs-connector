package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.jira.plugins.dvcs.pageobjects.util.PageElementUtils;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;
import static java.lang.String.format;

public class BitbucketAIDLoginPage implements Page {
    @ElementBy(cssSelector = "input[name=\"username\"]")
    private PageElement usernameOrEmailInput;

    @ElementBy(cssSelector = "input[name=\"password\"]")
    private PageElement passwordInput;

    @ElementBy(cssSelector = "#login-form .aui-button-primary")
    private PageElement loginButton;

    @ElementBy(id = "user-dropdown-trigger", timeoutType = PAGE_LOAD)
    private PageElement userDropdownTriggerLink;

    @ElementBy(id = "log-out-link")
    private PageElement logoutLink;

    @Inject
    private PageBinder pageBinder;

    @Override
    public String getUrl() {
        return "/account/signin/?next=/";
    }

    /**
     * Log in with the provided credentials.
     * <p>
     * If the provided username is already logged in, no action is performed.
     *
     * @param username The username to log in with
     * @param password The password to log in with
     * @throws IllegalStateException if a user is already logged in who is <em>not</em> the provided user
     */
    public void doLogin(String username, String password) {
        // accessing tag name as workaround for permission denied to access property 'nr@context' issue
        PageElementUtils.permissionDeniedWorkAround(usernameOrEmailInput);

        if (userIsLoggedIn()) {
            final String loggedInUser = getLoggedInUsername();
            if (!StringUtils.equals(loggedInUser, username)) {
                throw new IllegalStateException(format("Already logged in as '%s'. Make sure you log out first.", loggedInUser));
            }
            return;
        }

        usernameOrEmailInput.clear().type(username);
        passwordInput.clear().type(password);
        loginButton.click();
    }

    /**
     * Perform logout if there is a logged in user, and redirect to the Bitbucket homepage.
     *
     * @return The Bitbucket homepage
     */
    public BitbucketHomePage doLogout() {
        // accessing tag name as workaround for permission denied to access property 'nr@context' issue
        PageElementUtils.permissionDeniedWorkAround(usernameOrEmailInput);

        if (userIsLoggedIn()) {
            // only do the logout if the user drop down is present, i.e., if the user is logged in.
            userDropdownTriggerLink.click();
            logoutLink.click();
            return pageBinder.bind(BitbucketHomePage.class);
        }

        return pageBinder.navigateToAndBind(BitbucketHomePage.class);
    }

    private boolean userIsLoggedIn() {
        return userDropdownTriggerLink.isPresent();
    }

    private String getLoggedInUsername() {
        return userIsLoggedIn() ? userDropdownTriggerLink.getAttribute("title") : null;
    }
}
