package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.v2.ApiPaginatedResponse;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.v2.linker.ConnectLinkerValue;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.ResponseCallback;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import com.google.gson.reflect.TypeToken;
import org.apache.http.entity.ContentType;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils.fromJson;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Remote Restpoint for updating the linker module of a Connect based Organization
 */
@ParametersAreNonnullByDefault
public class ConnectLinkerRemoteRestpoint {

    private static final String BASE_LINKER_URL_PART = "/api/2.0/addon/linkers/dvcs-connector-issue-key-linker";
    @VisibleForTesting
    static final String VALUES_LINKER_URL = BASE_LINKER_URL_PART + "/values";
    private final RemoteRequestor requestor;

    public ConnectLinkerRemoteRestpoint(final RemoteRequestor remoteRequestor) {
        this.requestor = checkNotNull(remoteRequestor);
    }

    /**
     * Replace the linker values for the authenticated dvcs connector user in Bitbucket with the supplied values.
     *
     * @param values The new linker values for DVCS connector
     */
    public void replaceLinkers(final Iterable<String> values) {

        final Stream<String> iterableStream = StreamSupport.stream(values.spliterator(), false);
        final Stream<ConnectLinkerValue> linkersToCreate = iterableStream.map(ConnectLinkerValue::new);

        final Object[] linkersToCreateArray = linkersToCreate.toArray();
        if (linkersToCreateArray.length > 0) {
            final String json = ClientUtils.toJson(linkersToCreateArray);

            requestor.put(
                    VALUES_LINKER_URL,
                    json,
                    ContentType.APPLICATION_JSON,
                    ResponseCallback.EMPTY);
        } else {
            // DELETE is required if empty due to a bug in Bitbucket
            requestor.delete(VALUES_LINKER_URL,
                    ImmutableMap.of(),
                    ResponseCallback.EMPTY);
        }
    }

    /**
     * Retrieves the first page of linker values configured for DVCS connector
     *
     * @return The first page of linker values
     */
    public Iterable<ConnectLinkerValue> getLinkerValues() {
        ApiPaginatedResponse<ConnectLinkerValue> result = requestor.get(VALUES_LINKER_URL, null,
                response -> fromJson(response.getResponse(), new V2ApiPaginatedResponseTypeToken().getType()));
        return ApiPaginatedResponse.valuesOrEmptyList(result);
    }

    private static class V2ApiPaginatedResponseTypeToken extends TypeToken<ApiPaginatedResponse<ConnectLinkerValue>> {
    }
}
