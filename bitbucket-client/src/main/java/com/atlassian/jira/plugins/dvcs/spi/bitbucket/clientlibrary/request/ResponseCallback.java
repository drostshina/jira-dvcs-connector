package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request;

public interface ResponseCallback<T> {

    public static final ResponseCallback<Void> EMPTY = new ResponseCallback<Void>() {
        @Override
        public Void onResponse(RemoteResponse response) {
            return null;
        }

    };

    T onResponse(RemoteResponse response);
}

