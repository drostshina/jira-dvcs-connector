package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.v2;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Generic representation of the Bitbucket v2 api pages response wrapper
 */
public class ApiPaginatedResponse<T> {
    private int pagelen;
    private int page;
    private int size;
    private List<T> values;

    public static <T> List<T> valuesOrEmptyList(ApiPaginatedResponse<T> result) {
        return result == null || result.getValues() == null ? ImmutableList.of() : result.getValues();
    }

    public int getPagelen() {
        return pagelen;
    }

    public void setPagelen(int pagelen) {
        this.pagelen = pagelen;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<T> getValues() {
        return values;
    }

    public void setValues(List<T> values) {
        this.values = values;
    }
}
