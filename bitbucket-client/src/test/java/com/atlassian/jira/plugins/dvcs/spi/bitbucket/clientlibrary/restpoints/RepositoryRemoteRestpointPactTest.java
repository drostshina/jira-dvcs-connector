package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints;

import au.com.dius.pact.consumer.ConsumerPactBuilder;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactDslJsonBody;
import au.com.dius.pact.consumer.PactProviderRule;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.model.PactFragment;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.fusion.aci.api.service.exception.UninstalledException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepository;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BitbucketRequestException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.ACIJwtAuthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.NoAuthAuthProvider;
import com.google.common.collect.ImmutableMap;
import org.fest.assertions.core.Condition;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * PACT tests for the {@link RepositoryRemoteRestpoint}
 */
public class RepositoryRemoteRestpointPactTest {
    private static final String ACCOUNT_NAME = "dvcspact";
    private static final String NON_EXISTANT_ACCOUNT_NAME = "i-hope-this-doesnt-really-exist-" + System.currentTimeMillis();
    private static final String REPO_NAME = "pact_test";

    private static final String ACCOUNT_PATTERN = "[a-zA-Z0-9{}\\-\\._]+";
    private static final String REPOSITORY_SLUG_PATTERN = ACCOUNT_PATTERN;

    private static final String USERS_API_PATTERN = "(/!?api)?/1\\.0/users/" + ACCOUNT_PATTERN;
    private static final String USERS_API_EXAMPLE_PREFIX = "/api/1.0/users/";
    private static final String VALID_USERS_API_EXAMPLE = USERS_API_EXAMPLE_PREFIX + ACCOUNT_NAME;
    private static final String INVALID_USERS_API_EXAMPLE = USERS_API_EXAMPLE_PREFIX + NON_EXISTANT_ACCOUNT_NAME;

    private static final String REPOSITORIES_API_PATTERN = "(/!?api)?/1\\.0/repositories/" + ACCOUNT_PATTERN + "/" + REPOSITORY_SLUG_PATTERN;
    private static final String REPOSITORIES_API_EXAMPLE = "/api/1.0/repositories/" + ACCOUNT_NAME + "/" + REPO_NAME;

    private static final String JWT_TOKEN_PATTERN = "JWT \\w+\\.\\w+\\.\\w+";
    private static final String JWT_TOKEN_EXAMPLE = "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzY290Y2guaW8iLCJleHAiOjEzMDA4MTkzODAsIm5hbWUiOiJDaHJpcyBTZXZpbGxlamEiLCJhZG1pbiI6dHJ1ZX0.03f329983b86f7d9a9f5fef85305880101d5e302afafa20154d094b229f75773";

    private static final String SCM_PATTERN = "(hg|git)";
    private static final String SCM_EXAMPLE = "git";

    private static final String DESCRIPTION_PATTERN = ".*";
    private static final String DESCRIPTION_EXAMPLE = "A test repo";
    private final HttpClientProvider httpClientProvider = new HttpClientProvider();
    @Rule
    public PactProviderRule pactProvider = new PactProviderRule("bitbucket", this);
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private ACIJwtService jwtService;
    @Mock
    private ACIRegistrationService registrationService;
    @Mock
    private EventPublisher eventPublisher;
    private ACIJwtAuthProvider jwtAuthProvider;
    private NoAuthAuthProvider unauthenticatedProvider;

    private RepositoryRemoteRestpoint remoteRestClient;

    @Pact(provider = "bitbucket", consumer = "dvcs_connector")
    public PactFragment getReposWithInvalidJwtAccessFragment(ConsumerPactBuilder.PactDslWithProvider builder) {
        //@formatter:off
        return builder
                .given("org with private repos and jwt")
                .uponReceiving("GET repos with invalid JWT token")
                .method("GET")
                .matchPath(USERS_API_PATTERN, VALID_USERS_API_EXAMPLE)
                .headers(ImmutableMap.of("Authorization", JWT_TOKEN_EXAMPLE))
                // Commenting this line as the provider tests fail to use the regex - maybe a version compatibility issue
                //.matchHeader("Authorization", JWT_TOKEN_PATTERN, JWT_TOKEN_EXAMPLE)
                .willRespondWith()
                .status(401)
                .headers(ImmutableMap.of(
                        "Content-Type", "text/html; charset=utf-8"))
                .toFragment();
        //@formatter:on
    }

    @Pact(provider = "bitbucket", consumer = "dvcs_connector")
    public PactFragment getReposWithUnauthenticatedAccessFragment(ConsumerPactBuilder.PactDslWithProvider builder) {
        //@formatter:off
        return builder
                .given("org with public repos")
                .uponReceiving("GET repos with unauthenticated access")
                .method("GET")
                .matchPath(USERS_API_PATTERN, VALID_USERS_API_EXAMPLE)
                .willRespondWith()
                .status(200)
                .headers(ImmutableMap.of(
                        "Content-Type", "application/json; charset=utf-8"))
                .body(new PactDslJsonBody()
                        .eachLike("repositories")
                        .stringMatcher("owner", ACCOUNT_PATTERN, ACCOUNT_NAME)
                        .booleanType("is_fork", false)
                        .stringMatcher("slug", REPOSITORY_SLUG_PATTERN, REPO_NAME)
                        .stringMatcher("name", REPOSITORY_SLUG_PATTERN, REPO_NAME)
                        .stringMatcher("resource_uri", REPOSITORIES_API_PATTERN, REPOSITORIES_API_EXAMPLE)
                        .stringMatcher("description", DESCRIPTION_PATTERN, DESCRIPTION_EXAMPLE)
                        .booleanType("is_private", false)
                        .stringMatcher("scm", SCM_PATTERN, SCM_EXAMPLE)
                        .closeObject()
                        .closeArray()
                        .object("user")
                        .stringMatcher("username", ACCOUNT_PATTERN, ACCOUNT_NAME)
                        .closeObject()
                        .asBody())
                .toFragment();
        //@formatter:on
    }

    @Pact(provider = "bitbucket", consumer = "dvcs_connector")
    public PactFragment getReposWithInvalidAccountName(ConsumerPactBuilder.PactDslWithProvider builder) {
        //@formatter:off
        return builder
                .given("non-existent org")
                .uponReceiving("GET repos with invalid account name")
                .method("GET")
                .matchPath(USERS_API_PATTERN, INVALID_USERS_API_EXAMPLE)
                .willRespondWith()
                .status(404)
                .headers(ImmutableMap.of(
                        "Content-Type", "text/plain; charset=utf-8"))
                .toFragment();
        //@formatter:on
    }

    @Pact(provider = "bitbucket", consumer = "dvcs_connector")
    public PactFragment getRepoWithUnauthenticatedAccessFragment(ConsumerPactBuilder.PactDslWithProvider builder) {
        //@formatter:off
        return builder
                .given("org with public repos")
                .uponReceiving("GET repo with unauthenticated access")
                .method("GET")
                .matchPath(REPOSITORIES_API_PATTERN, REPOSITORIES_API_EXAMPLE)
                .willRespondWith()
                .status(200)
                .headers(ImmutableMap.of(
                        "Content-Type", "application/json; charset=utf-8"))
                .body(new PactDslJsonBody()
                        .stringMatcher("owner", ACCOUNT_PATTERN, ACCOUNT_NAME)
                        .booleanType("is_fork", false)
                        .stringMatcher("slug", REPOSITORY_SLUG_PATTERN, REPO_NAME)
                        .stringMatcher("name", REPOSITORY_SLUG_PATTERN, REPO_NAME)
                        .stringMatcher("resource_uri", REPOSITORIES_API_PATTERN, REPOSITORIES_API_EXAMPLE)
                        .stringMatcher("description", DESCRIPTION_PATTERN, DESCRIPTION_EXAMPLE)
                        .booleanType("is_private", false)
                        .stringMatcher("scm", SCM_PATTERN, SCM_EXAMPLE)
                        .asBody())
                .toFragment();
        //@formatter:on
    }

    @Before
    public void setup() throws Exception {
        jwtAuthProvider = new ACIJwtAuthProvider(
                pactProvider.getConfig().url(), "principal",
                jwtService, registrationService,
                httpClientProvider, eventPublisher);
        unauthenticatedProvider = new NoAuthAuthProvider(pactProvider.getConfig().url(), httpClientProvider);
    }

    @Test
    @PactVerification(value = "bitbucket", fragment = "getReposWithUnauthenticatedAccessFragment")
    public void testGetReposWithPublicReposAndAnonymousCredentials() {
        remoteRestClient = new RepositoryRemoteRestpoint(unauthenticatedProvider.provideRequestor());

        final List<BitbucketRepository> repositories = remoteRestClient.getAllRepositories(ACCOUNT_NAME);

        assertThat(repositories).have(new Condition<BitbucketRepository>() {
            @Override
            public boolean matches(final BitbucketRepository r) {
                return r.getName().equals(REPO_NAME) &&
                        r.isFork() == false &&
                        r.getSlug().equals(REPO_NAME) &&
                        r.getOwner().equals(ACCOUNT_NAME) &&
                        r.getResourceUri().equals(REPOSITORIES_API_EXAMPLE) &&
                        r.getDescription().equals(DESCRIPTION_EXAMPLE) &&
                        r.getScm().equals(SCM_EXAMPLE);
            }
        });
    }

    @Test(expected = BitbucketRequestException.Unauthorized_401.class)
    @PactVerification(value = "bitbucket", fragment = "getReposWithInvalidJwtAccessFragment")
    public void testGetReposWithInvalidJWTCredentials() throws UninstalledException {
        when(jwtService.generateJwtToken(any(String.class), any(String.class), eq("GET"), any(URL.class))).thenReturn(
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzY290Y2guaW8iLCJleHAiOjEzMDA4MTkzODAsIm5hbWUiOiJDaHJpcyBTZXZpbGxlamEiLCJhZG1pbiI6dHJ1ZX0.03f329983b86f7d9a9f5fef85305880101d5e302afafa20154d094b229f75773");
        remoteRestClient = new RepositoryRemoteRestpoint(jwtAuthProvider.provideRequestor());

        remoteRestClient.getAllRepositories(ACCOUNT_NAME);
    }

    @Test(expected = BitbucketRequestException.NotFound_404.class)
    @PactVerification(value = "bitbucket", fragment = "getReposWithInvalidAccountName")
    public void testGetReposWithInvalidAccount() {
        remoteRestClient = new RepositoryRemoteRestpoint(unauthenticatedProvider.provideRequestor());

        remoteRestClient.getAllRepositories(NON_EXISTANT_ACCOUNT_NAME);
    }

    @Test
    @PactVerification(value = "bitbucket", fragment = "getRepoWithUnauthenticatedAccessFragment")
    public void testGetRepoWithPublicReposAndAnonymousCredentials() {
        remoteRestClient = new RepositoryRemoteRestpoint(unauthenticatedProvider.provideRequestor());

        final BitbucketRepository repository = remoteRestClient.getRepository(ACCOUNT_NAME, REPO_NAME);

        assertThat(repository).isNotNull();
        assertThat(repository.getName()).isEqualTo(REPO_NAME);
        assertThat(repository.getOwner()).isEqualTo(ACCOUNT_NAME);
        assertThat(repository.getDescription()).isEqualTo(DESCRIPTION_EXAMPLE);
        assertThat(repository.getResourceUri()).isEqualTo(REPOSITORIES_API_EXAMPLE);
        assertThat(repository.getScm()).isEqualTo(SCM_EXAMPLE);
        assertThat(repository.getSlug()).isEqualTo(REPO_NAME);
        assertThat(repository.isFork()).isEqualTo(false);
    }
}
