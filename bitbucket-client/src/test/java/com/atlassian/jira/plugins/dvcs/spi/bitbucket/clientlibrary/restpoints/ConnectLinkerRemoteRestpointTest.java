package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.v2.linker.ConnectLinkerValue;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.ResponseCallback;
import com.google.common.collect.ImmutableList;
import org.apache.http.entity.ContentType;
import org.hamcrest.collection.IsArrayContainingInOrder;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.ConnectLinkerRemoteRestpoint.VALUES_LINKER_URL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConnectLinkerRemoteRestpointTest {
    private static final String LINKER_VALUE_JSON = "{\n" +
            "    'value': \"BB\",\n" +
            "    'links': {\n" +
            "        'self': {\n" +
            "            'href': \"https://bitbucket.org/api/2.0/addon/linkers/unique-module-key/values/23\"\n" +
            "        }\n" +
            "    }\n" +
            "}";

    @Mock
    private RemoteRequestor requestor;

    @Captor
    private ArgumentCaptor<String> jsonCaptor;

    @InjectMocks
    private ConnectLinkerRemoteRestpoint connectLinkerRemoteRestpoint;

    @BeforeClass
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void replaceLinkers_buildsCorrectLinkerValues_whenSuppliedWithKey() {
        // setup
        when(requestor.put(anyString(), jsonCaptor.capture(), any(ContentType.class), any(ResponseCallback.class))).thenReturn(null);
        final List<String> newValues = ImmutableList.of("ABC", "DEF");

        // execute
        connectLinkerRemoteRestpoint.replaceLinkers(newValues);

        // check
        final ConnectLinkerValue[] createdValues = ClientUtils.fromJson(jsonCaptor.getValue(), ConnectLinkerValue[].class);
        assertThat(createdValues, IsArrayContainingInOrder.arrayContaining(new ConnectLinkerValue("ABC"), new ConnectLinkerValue("DEF")));
    }

    @Test
    public void replaceLinkers_invokesDelete_whenSuppliedWithEmpty() {
        // setup
        when(requestor.delete(anyString(), any(Map.class), any(ResponseCallback.class))).thenReturn(new Object());

        // execute
        connectLinkerRemoteRestpoint.replaceLinkers(ImmutableList.of());

        // check
        verify(requestor).delete(anyString(), any(Map.class), any(ResponseCallback.class));
    }

    @Test
    public void getLinkers_invokesGet_whenInvoked() {
        // setup
        when(requestor.get(eq(VALUES_LINKER_URL), isNull(Map.class), any(ResponseCallback.class))).thenReturn(null);

        // execute
        Iterable<ConnectLinkerValue> retrievedValues = connectLinkerRemoteRestpoint.getLinkerValues();

        // verify
        assertThat(retrievedValues.iterator().hasNext(), equalTo(false));
    }

    @Test
    public void deserialiseJson_returnsLinkerValue_whenSuppliedWithLinkerValue() {
        final ConnectLinkerValue connectLinkerValue = ClientUtils.fromJson(LINKER_VALUE_JSON, ConnectLinkerValue.class);
        assertThat(connectLinkerValue.getValue(), equalTo("BB"));
    }

    @Test
    public void serialiseJson_returnsJson_whenOnlyValueSupplied() {
        final String value = "FOO";
        final ConnectLinkerValue connectLinkerValue = new ConnectLinkerValue(value);
        final String json = ClientUtils.toJson(connectLinkerValue);
        final ConnectLinkerValue parsedValue = ClientUtils.fromJson(json, ConnectLinkerValue.class);
        assertThat(parsedValue.getValue(), equalTo(connectLinkerValue.getValue()));
    }
}
