package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketService;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests over {@link BaseRemoteRequestor} implementation.
 */
public class BaseRemoteRequestorTest {

    private static final String INTERNAL_BITBUCKET_TEST_URI = "http://internal.bitbucket.org/api/test";

    /**
     * Tested object.
     */
    private BaseRemoteRequestor testedObject;

    private HttpResponse httpResponse;
    private StatusLine statusLine;

    /**
     * Mocked HTTP client - necessary to catch request execution.
     */
    @Mock
    private HttpClient httpClient;

    @Mock
    private HttpClientProvider httpClientProvider;

    @Mock
    private ClientConnectionManager connectionManager;

    /**
     * Captures performed requests.
     */
    @Captor
    private ArgumentCaptor<HttpUriRequest> httpUriRequest;

    @BeforeMethod
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);

        ApiProvider apiProvider = mock(ApiProvider.class);
        testedObject = new BaseRemoteRequestor(apiProvider, httpClientProvider);

        httpResponse = mock(HttpResponse.class);
        statusLine = mock(StatusLine.class);

        doReturn(connectionManager).when(httpClient).getConnectionManager();
        doReturn(httpResponse).when(httpClient).execute(Mockito.<HttpUriRequest>any());
        doReturn(new BasicHttpParams()).when(httpClient).getParams();
        doReturn(statusLine).when(httpResponse).getStatusLine();

        doReturn("http://bitbucket.org").when(apiProvider).getHostUrl();
        doReturn("http://bitbucket.org/api").when(apiProvider).getApiUrl();
        doReturn(httpClient).when(httpClientProvider).getHttpClient();
        doReturn(httpClient).when(httpClientProvider).getHttpClient(anyBoolean());
    }

    @Test(dataProvider = "relativeUriProvider")
    public void testRelativeUriAPI(final String uriPart, final String uri) throws Exception {
        @SuppressWarnings("unchecked")
        ResponseCallback<Void> callback = mock(ResponseCallback.class);
        testedObject.get(uriPart, Collections.emptyMap(), callback);
        verify(httpClient).execute(httpUriRequest.capture());

        assertThat(uri, is(httpUriRequest.getValue().getURI().toString()));
    }

    @DataProvider
    private Object[][] relativeUriProvider() {
        return new Object[][]{
                {"/test", "http://bitbucket.org/api/test"},
                {"/api/test", "http://bitbucket.org/api/test"},
                {INTERNAL_BITBUCKET_TEST_URI, INTERNAL_BITBUCKET_TEST_URI}
        };
    }

    @Test
    public void testCallbackIsCalledOnResponse() {
        @SuppressWarnings("unchecked")
        ResponseCallback<Void> callback = mock(ResponseCallback.class);

        testedObject.get(INTERNAL_BITBUCKET_TEST_URI, Collections.<String, String>emptyMap(), callback);
        verify(callback).onResponse(any(RemoteResponse.class));

        reset((Object) callback);
        testedObject.post(INTERNAL_BITBUCKET_TEST_URI, Collections.<String, String>emptyMap(), callback);
        verify(callback).onResponse(any(RemoteResponse.class));
    }

    @Test(expectedExceptions = BitbucketRequestException.BadRequest_400.class)
    public void testGetHttpErrorException() throws IOException {
        @SuppressWarnings("unchecked")
        ResponseCallback<Void> callback = mock(ResponseCallback.class);
        when(statusLine.getStatusCode()).thenReturn(400);

        testedObject.get(INTERNAL_BITBUCKET_TEST_URI, Collections.emptyMap(), callback);
    }

    @Test(expectedExceptions = BitbucketRequestException.BadRequest_400.class)
    public void testPostHttpErrorException() throws IOException {
        @SuppressWarnings("unchecked")
        ResponseCallback<Void> callback = mock(ResponseCallback.class);
        when(statusLine.getStatusCode()).thenReturn(400);

        testedObject.post(INTERNAL_BITBUCKET_TEST_URI, Collections.emptyMap(), callback);
    }

    @Test
    public void testCallbackIsNotCalledOnErrorResponse() throws IOException {
        @SuppressWarnings("unchecked")
        ResponseCallback<Void> callback = mock(ResponseCallback.class);

        when(statusLine.getStatusCode()).thenReturn(400);

        try {
            testedObject.get(INTERNAL_BITBUCKET_TEST_URI, Collections.emptyMap(), callback);
        } catch (BitbucketRequestException.BadRequest_400 ignored) {
        }

        try {
            testedObject.post(INTERNAL_BITBUCKET_TEST_URI, Collections.emptyMap(), callback);
        } catch (BitbucketRequestException.BadRequest_400 ignored) {
        }

        verify(callback, never()).onResponse(any(RemoteResponse.class));
    }

    @Test
    public void testCallbackDeserialization() throws IOException {
        HttpEntity entity = mock(HttpEntity.class);
        when(httpResponse.getEntity()).thenReturn(entity);

        InputStream is = IOUtils.toInputStream("{ fields: [ {name: \"URL\", \"value\": \"" + INTERNAL_BITBUCKET_TEST_URI + "\"} ] }");
        when(entity.getContent()).thenReturn(is);

        BitbucketService service = testedObject.get(INTERNAL_BITBUCKET_TEST_URI, Collections.emptyMap(),
                response -> ClientUtils.fromJson(response.getResponse(), BitbucketService.class));

        assertThat(service.getFields().get(0).getValue(), is(INTERNAL_BITBUCKET_TEST_URI));
    }

    @Test
    public void sanitizeContentForLogging_removesContent_whenHtmlTagFound() {
        final String content = "<html><head></head><body></body></html>";

        final String result = testedObject.sanitizeContentForLogging(content);

        assertThat(result, is(BaseRemoteRequestor.HTML_CONTENT_REPLACEMENT));
    }

    @Test
    public void sanitizeContentForLogging_removesContent_whenDocTypeTagFound() {
        final String content = "<!doctype html> <some><content/></some>";

        final String result = testedObject.sanitizeContentForLogging(content);

        assertThat(result, is(BaseRemoteRequestor.HTML_CONTENT_REPLACEMENT));
    }

    @Test
    public void sanitizeContentForLogging_leavesContent_whenNoHtmlFound() {
        final String content = "{foo:bar}";

        final String result = testedObject.sanitizeContentForLogging(content);

        assertThat(result, is(content));
    }

    @Test
    public void sanitizeContentForLogging_leavesContent_whenHtmlEmbeddedInPayload() {
        final String content = "{foo:\"<html>something</html>\"}";

        final String result = testedObject.sanitizeContentForLogging(content);

        assertThat(result, is(content));
    }

    @Test
    public void sanitizeHeadersForLogging_removesAuthorizationHeaders_whenPresent() {
        final Header[] headers = new Header[] {
                new BasicHeader("Content-Type", "text/html"),
                new BasicHeader("Authorization", "JWT 1234-5678"),
                new BasicHeader("Foo", "Bar"),
                new BasicHeader("X-Authorization-Custom", "Super secret")
        };

        final Header[] result = testedObject.sanitizeHeadersForLogging(headers);

        assertThat(result, arrayContaining(headers[0], headers[2]));
    }

    @Test
    public void sanitizeHeadersForLogging_removesNoHeaders_whenNoAuthHeadersPresent() {
        final Header[] headers = new Header[] {
                new BasicHeader("Content-Type", "text/html"),
                new BasicHeader("Foo", "Bar"),
        };

        final Header[] result = testedObject.sanitizeHeadersForLogging(headers);

        assertThat(result, arrayContaining(headers[0], headers[1]));
    }

    @Test
    public void sanitizeHeadersForLogging_handlesEmptyArray() {
        final Header[] headers = new Header[] {};

        final Header[] result = testedObject.sanitizeHeadersForLogging(headers);

        assertThat(result, emptyArray());
    }

    @Test(dataProvider = "exceptionProvider")
    public void checkAndCreateRemoteResponse_throwsCorrectException_forResponse(
            final int response, final Class<? extends BitbucketRequestException> exception) throws Exception {
        when(statusLine.getStatusCode()).thenReturn(response);
        try {
            testedObject.checkAndCreateRemoteResponse(new HttpGet(), httpResponse);
            throw new AssertionError("Expected an exception but didn't get one.");
        }
        catch (BitbucketRequestException e) {
            assertThat(e, instanceOf(exception));
        }
    }

    @DataProvider
    public Object[][] exceptionProvider() {
        return new Object[][]{
                {400, BitbucketRequestException.BadRequest_400.class},
                {401, BitbucketRequestException.Unauthorized_401.class},
                {403, BitbucketRequestException.Forbidden_403.class},
                {404, BitbucketRequestException.NotFound_404.class},
                {500, BitbucketRequestException.InternalServerError_500.class},
                {503, BitbucketRequestException.ServiceUnAvailable_503.class},
                {666, BitbucketRequestException.Other.class}
        };
    }

    @Test
    public void getSentryId_returnsSentryHeader_ifPresent() {
        when(httpResponse.getFirstHeader(eq("X-Sentry-ID"))).thenReturn(new BasicHeader("X-Sentry-ID", "foo"));

        assertThat(testedObject.getSentryId(httpResponse).get(), is("foo"));
    }

    @Test
    public void getSentryId_returnsNull_ifNotPresent() {
        when(httpResponse.getFirstHeader(eq("X-Sentry-ID"))).thenReturn(null);

        assertThat(testedObject.getSentryId(httpResponse).isPresent(), is(false));
    }
}
