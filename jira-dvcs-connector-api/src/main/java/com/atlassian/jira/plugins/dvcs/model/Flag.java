package com.atlassian.jira.plugins.dvcs.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A simple boolean flag type
 */
@XmlRootElement
public class Flag {

    public static Flag of(boolean value) {
        return new Flag(value);
    }

    @XmlElement
    private boolean value;

    public Flag(boolean value) {
        this.value = value;
    }

    public Flag() {
    }

    public boolean getValue() {
        return value;
    }
}
