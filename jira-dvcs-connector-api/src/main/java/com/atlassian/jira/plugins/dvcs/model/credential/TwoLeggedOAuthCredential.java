package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Credentials for 2LO authentication.
 * <p>
 * Provides key and secret.
 */
public class TwoLeggedOAuthCredential implements OAuthCredential {
    private final String oauthKey;
    private final String oauthSecret;

    public TwoLeggedOAuthCredential(@Nonnull final String oauthKey, @Nonnull final String oauthSecret) {
        checkArgument(isNotBlank(oauthKey));
        checkArgument(isNotBlank(oauthSecret));

        this.oauthKey = oauthKey;
        this.oauthSecret = oauthSecret;
    }

    public static CredentialVisitor<Optional<TwoLeggedOAuthCredential>> visitor() {
        return new TwoLeggedOauthCredentialVisitor();
    }

    @Override
    @Nonnull
    public String getKey() {
        return oauthKey;
    }

    @Override
    @Nonnull
    public String getSecret() {
        return oauthSecret;
    }

    @Override
    @Nonnull
    public CredentialType getType() {
        return CredentialType.TWO_LEGGED_OAUTH;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TwoLeggedOAuthCredential that = (TwoLeggedOAuthCredential) o;
        return Objects.equals(oauthKey, that.oauthKey) &&
                Objects.equals(oauthSecret, that.oauthSecret);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oauthKey, oauthSecret);
    }

    @Override
    public <T> T accept(CredentialVisitor<T> visitor) {
        return visitor.visit(this);
    }

    private static class TwoLeggedOauthCredentialVisitor
            extends AbstractOptionalCredentialVisitor<TwoLeggedOAuthCredential> {
        @Override
        public Optional<TwoLeggedOAuthCredential> visit(final TwoLeggedOAuthCredential credential) {
            return Optional.ofNullable(credential);
        }
    }
}
