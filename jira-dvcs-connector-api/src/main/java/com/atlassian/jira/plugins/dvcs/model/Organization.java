package com.atlassian.jira.plugins.dvcs.model;

import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static org.apache.commons.lang3.StringUtils.defaultString;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Organization implements Serializable {
    public static final String GROUP_SLUGS_SEPARATOR = ";";

    private int id;
    private String hostUrl;
    private String name;
    private String dvcsType;
    private boolean autolinkNewRepos;
    private boolean smartcommitsOnNewRepos;
    private String organizationUrl;
    private List<Repository> repositories;
    private ApprovalState approvalState = ApprovalState.APPROVED;
    private transient Credential credential;

    // 2/ invitation groups - when adding new user as information holder for rendering form extension
    private transient List<Group> groups;

    // 1/ default groups - when configuring default groups
    private transient Set<Group> defaultGroups;

    public Organization() {
        super();
    }

    public Organization(int id, String hostUrl, String name, String dvcsType,
                        boolean autolinkNewRepos, Credential credential, String organizationUrl,
                        boolean smartcommitsOnNewRepos, Set<Group> defaultGroups) {
        this.id = id;
        this.hostUrl = hostUrl;
        this.name = name;
        this.dvcsType = dvcsType;
        this.autolinkNewRepos = autolinkNewRepos;
        this.approvalState = ApprovalState.APPROVED;
        this.credential = credential;
        this.organizationUrl = organizationUrl;
        this.smartcommitsOnNewRepos = smartcommitsOnNewRepos;
        this.defaultGroups = defaultGroups;
    }

    /**
     * Clone the provided organization. Note repositories won't be copied across.
     *
     * @param other original organization to be cloned
     */
    public Organization(Organization other) {
        this(other.id, other.hostUrl, other.name, other.dvcsType, other.autolinkNewRepos, null,
                other.organizationUrl, other.smartcommitsOnNewRepos, null);

        this.credential = other.credential;
        this.setApprovalState(other.getApprovalState());

        // Note: it is fine to use shallow copy for defaultGroups/groups because Group cannot be changed.
        // Ideally these collections should be immutable but there are existing codes modifying them (e.g. sorting)
        this.defaultGroups = other.defaultGroups != null ? Sets.newHashSet(other.defaultGroups) : null;
        this.groups = other.groups != null ? Lists.newArrayList(other.groups) : null;
    }

    // =============== getters ==========================
    public int getId() {
        return id;
    }

    // =============== setters ==========================
    public void setId(int id) {
        this.id = id;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public void setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDvcsType() {
        return dvcsType;
    }

    public void setDvcsType(String dvcsType) {
        this.dvcsType = dvcsType;
    }

    public boolean isAutolinkNewRepos() {
        return autolinkNewRepos;
    }

    public void setAutolinkNewRepos(boolean autolinkNewRepos) {
        this.autolinkNewRepos = autolinkNewRepos;
    }

    /**
     * Returns approval state for this Organization. If the approval state is not 'APPROVED',
     * no syncing will happen for Repos in this Organization.
     *
     * @return approval state for this organization.
     * @see com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState
     */
    public ApprovalState getApprovalState() {
        return approvalState;
    }

    public void setApprovalState(final ApprovalState approvalState) {
        this.approvalState = approvalState;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getOrganizationUrl() {
        return organizationUrl;
    }

    public void setOrganizationUrl(String organizationUrl) {
        this.organizationUrl = organizationUrl;
    }

    public Set<Group> getDefaultGroups() {
        return defaultGroups;
    }

    public void setDefaultGroups(Set<Group> defaultGroups) {
        this.defaultGroups = defaultGroups;
    }

    public boolean isSmartcommitsOnNewRepos() {
        return smartcommitsOnNewRepos;
    }

    public void setSmartcommitsOnNewRepos(boolean smartcommitsOnNewRepos) {
        this.smartcommitsOnNewRepos = smartcommitsOnNewRepos;
    }

    /**
     * Retrieve the repositories for this organization, note that this field is not always populated depending on
     * how the Organization was retrieved. Keep this in mind when using
     *
     * @return The repositories for this Organization _if_ they have been populated.
     */
    public List<Repository> getRepositories() {
        return repositories.stream().sorted((repo1, repo2) -> defaultString(repo1.getName()).toLowerCase()
                .compareTo(defaultString(repo2.getName()).toLowerCase())).collect(toImmutableList());
    }

    public boolean hasLinkedRepositories() {
        return repositories.stream().filter(Repository::isLinked).findFirst().isPresent();
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Organization that = (Organization) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(hostUrl, that.hostUrl)
                .append(name, that.name)
                .append(dvcsType, that.dvcsType)
                .append(credential, that.credential)
                .append(approvalState, that.approvalState)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(hostUrl)
                .append(name)
                .append(dvcsType)
                .append(credential)
                .append(approvalState)
                .hashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("hostUrl", hostUrl)
                .append("name", name)
                .append("dvcsType", dvcsType)
                .append("autolinkNewRepos", autolinkNewRepos)
                .append("smartcommitsOnNewRepos", smartcommitsOnNewRepos)
                .append("organizationUrl", organizationUrl)
                .append("repositories", repositories)
                .append("credential", credential)
                .append("groups", groups)
                .append("defaultGroups", defaultGroups)
                .append("approvalState", approvalState)
                .toString();
    }

    public boolean isIntegratedAccount() {
        final Optional<TwoLeggedOAuthCredential> maybeTwoLeggedCredential = Optional.ofNullable(credential)
                .flatMap(credential -> credential.accept(TwoLeggedOAuthCredential.visitor()));
        return maybeTwoLeggedCredential.isPresent();
    }

    /**
     * @return true if this Organization is using Principal ID based authentication
     * (i.e. it is managed by ACI), otherwise false.
     */
    public boolean isUsingPrincipalIdBasedAuthentication() {
        return getCredential().accept(PrincipalIDCredential.visitor()).isPresent();
    }

    /**
     * Represents approval state of a Dvcs Organization. If in APPROVED state,
     * it means that a JIRA administrator has approved the connection
     * between a DVCS account (e.g. BB account) and this particular Organization
     */
    public enum ApprovalState {
        /**
         * If Organization is in this state, it means that it is Connect managed Organization that was explicitly
         * approved by JIRA Admin, or it is non Connect managed Organization. This state is used by default
         * for non Connect managed Organizations.
         */
        APPROVED,

        /**
         * The Dvcs Organization was set up by JIRA User or JIRA Admin who did not finish the approval process.
         * In this state, no syncing will occur for any Repo in this Organization.
         */
        PENDING_APPROVAL
    }
}
