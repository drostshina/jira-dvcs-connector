package com.atlassian.jira.plugins.dvcs.analytics.smartcommits.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Nonnull;

public class SmartCommitRepoConfigChangedEvent {
    private final int repoID;
    private final boolean smartCommitEnabledByDefault;
    private final String BASE_EVENT_NAME = "jira.dvcsconnector.smartcommit.repo.%s";

    public SmartCommitRepoConfigChangedEvent(@Nonnull final int repoID, @Nonnull final boolean smartCommitEnabledByDefault) {
        Preconditions.checkNotNull(repoID);
        Preconditions.checkNotNull(smartCommitEnabledByDefault);
        this.repoID = repoID;
        this.smartCommitEnabledByDefault = smartCommitEnabledByDefault;
    }

    @EventName
    public String determineEventName() {
        return String.format(BASE_EVENT_NAME, smartCommitEnabledByDefault ? "enabled" : "disabled");
    }

    public int getrepoId() {
        return this.repoID;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}

