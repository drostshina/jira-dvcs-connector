package com.atlassian.jira.plugins.dvcs.analytics.smartcommits;

import com.atlassian.analytics.api.annotations.EventName;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Nonnull;

public class AccountAddedWithSmartCommitsEvent {

    private static final String EVENT_BASE_NAME = "jira.dvcsconnector.%s.smartcommit.default.enabled";
    private final com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType dvcsType;

    public AccountAddedWithSmartCommitsEvent(@Nonnull final com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType dvcsType) {
        Preconditions.checkNotNull(dvcsType);
        this.dvcsType = dvcsType;
    }

    @EventName
    public String determineEventName() {
        return String.format(EVENT_BASE_NAME, dvcsType);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}


