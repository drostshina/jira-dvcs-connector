package com.atlassian.jira.plugins.dvcs.model;


/**
 * Describes the type of file action within a {@link Changeset changeset}.
 */
public enum ChangesetFileAction {
    ADDED("added", "green"), REMOVED("removed", "red"), MODIFIED("modified", "blue");

    private final String color;
    private String action;

    ChangesetFileAction(String action, String color) {
        this.action = action;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public String getAction() {
        return action;
    }
}
