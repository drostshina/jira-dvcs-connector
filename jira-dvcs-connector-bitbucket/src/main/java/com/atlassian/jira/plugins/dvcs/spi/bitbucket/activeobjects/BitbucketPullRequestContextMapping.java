package com.atlassian.jira.plugins.dvcs.spi.bitbucket.activeobjects;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

import java.util.Date;

@Table("BITBUCKET_PR_CONTEXT")
public interface BitbucketPullRequestContextMapping extends Entity {
    String REMOTE_PULL_REQUEST_ID = "REMOTE_PULL_REQUEST_ID";
    String REPOSITORY_ID = "REPOSITORY_ID";
    String LOCAL_PULL_REQUEST_ID = "LOCAL_PULL_REQUEST_ID";
    String SAVED_UPDATE_ACTIVITY = "SAVED_UPDATE_ACTIVITY";
    String COMMITS_URL = "COMMITS_URL";
    String NEXT_COMMIT = "NEXT_COMMIT";

    // Last update activity
    String LAST_UPDATE_ACTIVITY_STATUS = "LAST_ACTIVITY_STATUS";
    String LAST_UPDATE_ACTIVITY_DATE = "LAST_ACTIVITY_DATE";
    String LAST_UPDATE_ACTIVITY_AUTHOR = "LAST_ACTIVITY_AUTHOR";
    String LAST_UPDATE_ACTIVITY_RAW_AUTHOR = "LAST_ACTIVITY_RAW_AUTHOR";

    long getRemotePullRequestId();

    //
    // setters
    //
    void setRemotePullRequestId(long pullRequestId);

    int getRepositoryId();

    void setRepositoryId(int repositoryId);

    int getLocalPullRequestId();

    void setLocalPullRequestId(int localPullRequestId);

    boolean isSavedUpdateActivity();

    void setSavedUpdateActivity(boolean isSavedUpdateActivity);

    String getCommitsUrl();

    void setCommitsUrl(String commitsUrl);

    String getNextCommit();

    void setNextCommit(String node);

    String getLastActivityStatus();

    void setLastActivityStatus(String status);

    Date getLastActivityDate();

    void setLastActivityDate(Date date);

    String getLastActivityAuthor();

    void setLastActivityAuthor(String username);

    String getLastActivityRawAuthor();

    void setLastActivityRawAuthor(String rawAuthor);
}
