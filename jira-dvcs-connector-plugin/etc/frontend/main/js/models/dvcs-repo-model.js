define('jira-dvcs-connector/bitbucket/models/dvcs-repo-model', [
    'jira-dvcs-connector/lib/backbone',
    'jquery',
    'jira-dvcs-connector/util/window'
], function (Backbone,
             $,
             window) {
    "use strict";

    return Backbone.Model.extend({
        idAttribute: "id",

        enable: function () {
            this.addOrRemoveRepo(true);
        },

        sync: function () {

        },

        isEnabled: function () {
            return this.get('linked');
        },

        isBusy: function () {
            return !!this.busy;
        },

        disable: function () {
            this.addOrRemoveRepo(false);
        },

        getName: function () {
            return this.get('name');
        },

        hasPermissionError: function () {
            // check first if last registration exist
            var registration = this.getLastRegistration();
            if (registration) {
                return !registration.callBackUrlInstalled;
            }
            // return initial state
            var syncStatus = this.get('sync');
            return !!syncStatus && !syncStatus.hasAdminPermission;
        },

        getLastRegistration: function () {
            return this.get('registration');
        },

        getWebhookCallbackUrl: function () {
            return window.location.origin + window.BASE_URL + '/rest/bitbucket/1.0/repository/' + this.id + '/sync';
        },

        getLastError: function () {
            return this.get('error');
        },

        getSmartcommitsEnabled: function () {
            return this.get('smartcommitsEnabled');
        },

        addOrRemoveRepo: function (enable) {
            this.busy = true;
            var repoId = this.id;
            var self = this;
            $.ajax(
                {
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json",
                    url: window.BASE_URL + "/rest/bitbucket/1.0/repo/" + repoId + "/autolink",

                    data: JSON.stringify({
                        payload: enable
                    }),

                    success: function (registration) {
                        this.set({
                            linked: enable,
                            registration: registration,
                            error: undefined,
                            smartcommitsEnabled: registration.repository.smartcommitsEnabled
                        });
                        this.busy = false;
                    }.bind(this)
                }
            ).error(function (err) {
                this.set({
                    linked: enable,
                    registration: undefined,
                    error: err
                });
                this.busy = false;
            }.bind(this));
        }
    });

});

