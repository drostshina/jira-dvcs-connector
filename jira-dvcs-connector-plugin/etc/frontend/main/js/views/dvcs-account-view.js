/**
 * @module represent single connected dvcs account
 */
define('jira-dvcs-connector/bitbucket/views/dvcs-account-view', [
    'jquery',
    "underscore",
    'wrm/context-path',
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/bitbucket/views/dvcs-repo-multi-selector',
    'jira-dvcs-connector/bitbucket/views/dvcs-repos-table-view',
    'jira-dvcs-connector/bitbucket/views/confirmation-dialog',
    'jira-dvcs-connector/bitbucket/models/dvcs-repo-collection',
    'jira-dvcs-connector/bitbucket/views/webhooks-dialog',
    'jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-org-container-view',
    'jira-dvcs-connector/rest/dvcs-connector-rest-client',
    'jira-dvcs-connector/util/constants',
    'jira-dvcs-connector/util/window',
    'jira-dvcs-connector/bitbucket/views/dvcs-account-settings'
], function ($,
             _,
             contextPath,
             Backbone,
             RepoMultiSelector,
             ReposTable,
             ConfirmationDialog,
             RepoCollection,
             WebhooksDialog,
             AdminPageOrgContainer,
             RestClient,
             Constants,
             window,
             DVCSaccountSettings) {
    "use strict";

    var restClient = new RestClient(contextPath());

    return Backbone.View.extend({

        emptyDivSelector: '.no-enabled-repos',

        events: {
            "click .dvcs-header-container": '_toggleAccountPanel'
        },

        initialize: function () {
            this.repos = new RepoCollection();
            this.repos.on('change:linked', this._update, this);
            this.repos.on('add', this._update, this);

            this._bindDomElements();

            if (this.expandable) {
                this._updateHeaderExpanderIcon();
            }
            this.emptyAccountTemplate = dvcs.connector.plugin.soy.dvcs.accounts.emptyAccount;
            this.orgContainer = new AdminPageOrgContainer({el: this.el});
            this.orgContainer.on("jira-dvcs-connector:pending-org-deleted", this._removeOrg, this);
            this.settings = new DVCSaccountSettings({
                el: '#setting-dropdown-' + this.model.getId(),
                model: this.model
            });
        },

        render: function () {
            var orgId = this.model.getId();
            if (this.model.hasLinkedRepositories()) {
                // show spinner until we load linked repos
                this.emptyDiv.spin('large');
            } else {
                // show empty div content
                this._showEmptyState();
            }

            restClient.organization.getRepositories(orgId)
                .done(this._addDataLazily.bind(this))
                .fail(function (err, textStatus, errorThrown) {
                    console.error('could not retrieve repositories list for account "' + this.model.getName() + '"');
                }.bind(this));

            this._renderReposTable();
            this._renderMultiSelector();
            // show the add repo multi selector form after rendering completed
            this.$el.find('form.approved-org').show();
        },

        _renderMultiSelector: function () {
            this.repoMultiSelector = new RepoMultiSelector({
                el: this.$el.find('form'),
                collection: this.repos
            });
            // fired when one/more repos has been completely added
            // used to display webhook warning dialog
            this.repoMultiSelector.on('jira-dvcs-connector:repos-batch-added', this._showWebhookWarningDialog, this.repoMultiSelector);
            this.repoMultiSelector.render();
        },

        _renderReposTable: function () {
            var tableSelector = '#' + this.$el.attr("id") + ' .dvcs-repos-table';
            this.reposTable = new ReposTable({
                el: tableSelector,
                collection: this.repos,
                syncDisabled: this.syncDisabled,
                accountModel: this.model
            });
            this.reposTable.render();
        },

        _update: function () {
            this.model.setHasLinkedRepositories(this.repos.hasEnabledRepos());
            this._showEmptyState();
        },

        _showEmptyState: function () {
            if (this.model.hasLinkedRepositories()) {
                this.emptyDiv.html('');
                this.emptyDiv.hide();
            } else if (this.model.isApproved()) {
                this.emptyDiv.html(this.emptyAccountTemplate());
                this.emptyDiv.show();
            }
        },

        _bindDomElements: function () {
            this.emptyDiv = this.$el.find(this.emptyDivSelector);

            this.header = this.$el.find('.dvcs-header-container');
            this.expandable = this.header.hasClass('expandable');
            this.body = this.$el.find('div[id^="dvcs-account-repos-"]');
            this.syncDisabled = this.body.data('sync-disabled');
            this.expanderIcon = this.header.find('h4');

            this.deleteOrgLink = this.$el.find('.dvcs-control-delete-org');
            this.deleteOrgLink.on('click', this._deleteOrganization.bind(this));
        },

        _toggleAccountPanel: function (eventObject) {
            if (this.expandable && eventObject &&
                !$(eventObject.target).is(".accountSettingsButton, button, a")) {
                this.body.toggle();
                this._updateHeaderExpanderIcon();
            }
        },

        _updateHeaderExpanderIcon: function () {
            if (this.body.is(":visible")) {
                this.expanderIcon.removeClass("aui-iconfont-collapsed");
                this.expanderIcon.addClass("aui-iconfont-expanded");
            } else {
                this.expanderIcon.removeClass("aui-iconfont-expanded");
                this.expanderIcon.addClass("aui-iconfont-collapsed");
            }
        },

        expandBody: function () {
            if (!this.body.is(":visible")) {
                this.body.toggle();
                this._updateHeaderExpanderIcon();
            }
        },

        removeExpandable: function () {
            this.header.removeClass('expandable');
            this.expandable = false;
            this.$el.find('h4').removeClass('expandable');
        },

        _deleteOrganization: function () {
            var self = this;
            var confirmDialog = new ConfirmationDialog({
                header: "Deleting Account '" + self.model.getName() + "'",
                body: dvcs.connector.plugin.soy.confirmDelete({'organizationName': self.model.getName()}),
                submitButtonLabel: "Delete",
                okAction: function (dialog) {
                    self._deleteOrganizationInternal(dialog, self.model.getId(), self.model.getName());
                }
            });
            confirmDialog.render();
        },

        _deleteOrganizationInternal: function (dialog, organizationId, organizationName) {
            var self = this;
            restClient.organization.remove(organizationId)
                .done(function () {
                    self._removeOrg();
                    dialog.remove();
                })
                .fail(function (err, textStatus, errorThrown) {

                    // If we get a 404, assume it is already removed;
                    // If we get a timeout, assume deletion is continuing on the server so remove org anyway;
                    // Any other error, show the unknown error screen.
                    if (err.status === 404 || err.status === 0) {
                        dialog.remove();
                        self._removeOrg();
                    }
                    else {
                        dialog.showError(AJS.I18n.getText('dvcs.accounts.page.account.view.delete.org.error.generic', organizationName));
                    }
                });
        },

        _removeOrg: function () {
            this.$el.remove();
            this.trigger('jira-dvcs-connector:org-deleted', this.model.toJSON());
            this.off();
            this.undelegateEvents();
            this.remove();
        },

        _showWebhookWarningDialog: function (repos) {
            var repoWebhookUrls = {};
            repos && repos.filter(function (repo) {
                var registration = repo.getLastRegistration();
                return registration !== null && !registration.callBackUrlInstalled;
            }).map(function (repo) {
                repoWebhookUrls[repo.id] = {
                    repoName: repo.getName(),
                    callBackUrl: repo.getLastRegistration().callBackUrl
                };
            });
            if (_.keys(repoWebhookUrls).length) {
                WebhooksDialog.showWebhooksDialog(this.$el.attr("id"), repoWebhookUrls);
            }
        },

        _addDataLazily: function(data) {
            if (data && data.length) {
                data.sort((repo1, repo2) => repo1.name.toLowerCase() < repo2.name.toLowerCase() ? -1 : 1);
                var linked = [], unlinked = [];
                data.forEach(function (repo) {
                    repo.linked ? linked.push(repo) : unlinked.push(repo);
                });
                this.repos.add(linked);
                this._addUnLinkedRepos(unlinked, Constants.REPOSITORIES_CHUNK_SIZE);
                this.emptyDiv.spinStop();
            }
        },

        _addUnLinkedRepos: function(unlinked, chunkSize) {
            if (unlinked.length > chunkSize) {
                var processed = 0;
                this.repoMultiSelector.spin();
                var interval = window.setInterval(function() {
                    if (processed < unlinked.length) {
                        var chunk =  unlinked.slice(processed, processed + chunkSize);
                        processed += chunk.length;
                        this.repos.add(chunk);
                    } else {
                        this.repoMultiSelector.spinStop();
                        window.clearInterval(interval);
                    }
                }.bind(this), Constants.REPOSITORIES_RENDERING_INTERVAL);
            } else {
                this.repos.add(unlinked);
            }
        }
    });
});