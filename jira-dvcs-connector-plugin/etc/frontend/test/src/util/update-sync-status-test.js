define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'fusion/test/jquery'
], function (QUnit,
             __,
             $) {

    var tooltipSpy = $.fn.tooltip = sinon.spy(function (arg) {
        return arg;
    });

    var templateSpy = sinon.spy(function (args) {
        return "template contents";
    });

    QUnit.module('jira-dvcs-connector/ui/update-sync-status', {
        require: {
            main: 'jira-dvcs-connector/ui/update-sync-status',
            jquery: 'fusion/test/jquery'
        },
        templates: {
            'dvcs.connector.plugin.soy.sync_error': templateSpy,
            'dvcs.connector.plugin.soy.sync_progress': templateSpy
        },
        mocks: {
            jquery: QUnit.moduleMock('jquery', function () {
                return $;
            })
        },

        beforeEach: function (assert, update_sync_status) {
            this.fixture.append('<span id="syncrepoicon_1"  class="syncrepoicon" title="title" data-title="data-title" data-last-sync="">Click to sync repository</span>');
            this.fixture.append('<span id="syncicon_1">syncicon_1</span>');
            this.fixture.append('<span id="sync_status_message_1">sync_status_message_1</span>');
            this.fixture.append('<span id="sync_error_message_1">sync_error_message_1</span>');
        },

        afterEach: function () {
            templateSpy.reset();
            tooltipSpy.reset();
        }

    });

    QUnit.test('Sync Finished No Error', function (assert, update_sync_status) {
        var idNum = 1;
        var finishedTime = 1451606400;
        var finishSyncDateTime = new Date(finishedTime);
        var errorText = "errorText";
        var errorTitle = "errorTitle";
        var repo = {
            id: idNum,
            sync: {
                finished: true,
                finishTime: finishedTime,
                error: errorText,
                errorTitle: errorTitle,
                warning: false,
            }
        };


        update_sync_status(repo, sinon.spy());

        assert.assertThat('last-sync data is stored', this.fixture.find('#syncrepoicon_1').data('last-sync'), __.equalTo(finishedTime));
        assert.assertThat('last sycn information appended to title', this.fixture.find('#syncrepoicon_1').attr('title'), __.equalTo("data-title (last sync finished at " + finishSyncDateTime.toDateString() + " " + finishSyncDateTime.toLocaleTimeString() + ")"));
        assert.assertThat('sync status div should be empty', this.fixture.find('#sync_status_message_' + repo.id).html(), __.equalTo(""));


        //test that addHoverIcon works correctly
        assert.assertThat('aui-icon was added', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-icon"), __.equalTo(true));
        assert.assertThat('aui-icon-small was added', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-icon-small"), __.equalTo(true));
        assert.assertThat('aui-iconfont-refresh-small', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-iconfont-refresh-small"), __.equalTo(true));
        assert.assertThat('hover_icon', this.fixture.find('#syncrepoicon_' + repo.id).is(".hover_icon"), __.equalTo(true));
        assert.assertThat('title set to data-title value', this.fixture.find('#syncrepoicon_' + repo.id).text(), __.equalTo("data-title"));
    });

    QUnit.test('Sync Finished with Error', function (assert, update_sync_status) {
        var idNum = 1;
        var finishedTime = 1451606400;
        var errorText = "errorText";
        var errorTitle = "errorTitle";
        var repo = {
            id: idNum,
            sync: {
                finished: true,
                finishTime: finishedTime,
                error: errorText,
                errorTitle: errorTitle,
                warning: false,
            }
        };

        update_sync_status(repo, sinon.spy());

        assert.assertThat('error template should be called', templateSpy.callCount, __.equalTo(1));
        assert.assertThat('template status should be called with expected arguments', templateSpy.getCall(0).args[0], __.equalTo({
            "syncIcon": "error",
            "syncTitle": errorTitle,
            "syncError": errorText
        }));
        assert.assertThat('sync status div should be empty', this.fixture.find('#sync_status_message_' + repo.id).html(), __.equalTo(""));

        //test that addHoverIcon works correctly
        assert.assertThat('aui-icon was added', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-icon"), __.equalTo(true));
        assert.assertThat('aui-icon-small was added', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-icon-small"), __.equalTo(true));
        assert.assertThat('aui-iconfont-refresh-small', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-iconfont-refresh-small"), __.equalTo(true));
        assert.assertThat('hover_icon', this.fixture.find('#syncrepoicon_' + repo.id).is(".hover_icon"), __.equalTo(true));
        assert.assertThat('title set to data-title value', this.fixture.find('#syncrepoicon_' + repo.id).text(), __.equalTo("data-title"));
    });

    QUnit.test('Sync in progress Finished with Error', function (assert, update_sync_status) {
        var idNum = 1;
        var finishedTime = 1451606400;
        var startTime = finishedTime - 1000;
        var startSyncDateTime = new Date(startTime);
        var finishSyncDateTime = new Date(finishedTime);
        var errorText = "errorText";
        var errorTitle = "errorTitle";
        var changeSetCount = 1;
        var pullRequestActivityCount = 2;
        var jiraCount = 3;

        var repo = {
            id: idNum,
            sync: {
                finished: false,
                startTime: startTime,
                finishTime: finishedTime,
                synchroErrorCount: [1],
                errorTitle: errorTitle,
                warning: false,
                changesetCount: changeSetCount,
                pullRequestActivityCount: pullRequestActivityCount,
                jiraCount: jiraCount
            }
        };

        update_sync_status(repo, sinon.spy());

        //Synchronizing...
        assert.assertThat('In progress sync has expected title', this.fixture.find('#syncrepoicon_' + repo.id).attr('title'),
            __.equalTo("Synchronizing... (started at " + startSyncDateTime.toDateString() + " " + startSyncDateTime.toLocaleTimeString() + ")"));

        assert.assertThat('template  should be called with expected arguments', templateSpy.getCall(0).args[0], __.equalTo({
            "changesetCount": changeSetCount,
            "pullRequestCount": pullRequestActivityCount,
            "issueCount": jiraCount
        }));
        assert.assertThat('sync status div should contain', this.fixture.find('#sync_status_message_' + repo.id).html(),
            __.equalTo("template contents" + ", <span style=\"color:#e16161;\"><strong>1</strong>com.atlassian.jira.plugins.dvcs.admin.sync.status.commits-incomplete</span>"));


    });

    QUnit.test('addHoverIcon works on finished sync', function (assert, update_sync_status) {
        var idNum = 1;
        var errorText = "errorText";
        var errorTitle = "errorTitle";
        var repo = {
            id: idNum,
            sync: {
                finished: true,
            }
        };

        this.fixture.find('#syncrepoicon_1').addClass("refresh_running");
        assert.assertThat('refresh_running was present', this.fixture.find('#syncrepoicon_' + repo.id).is(".refresh_running"), __.equalTo(true));

        update_sync_status(repo, sinon.spy());

        containsHoverIconClasses(assert, __, this.fixture, repo.id);
        assert.assertThat('refresh_running was removed', this.fixture.find('#syncrepoicon_' + repo.id).is(".refresh_running"), __.equalTo(false));

    });

    QUnit.test('removeHoverIcon works on in progess sync', function (assert, update_sync_status) {
        //setup
        var idNum = 1;

        var repo = {
            id: idNum,
            sync: {
                finished: false,
            }
        };

        this.fixture.find('#syncrepoicon_1').addClass("hover_icon aui-icon aui-icon-small aui-iconfont-refresh-small");
        containsHoverIconClasses(assert, __, this.fixture, repo.id);

        //do
        update_sync_status(repo, sinon.spy());
        ;

        //check
        assert.assertThat('refresh_running was added', this.fixture.find('#syncrepoicon_' + repo.id).is(".refresh_running"), __.equalTo(true));
        assert.assertThat('non breaking space added', this.fixture.find('#syncrepoicon_' + repo.id).text(), __.equalTo('\xa0'));

        assert.assertThat('hover_icon removed', this.fixture.find('#syncrepoicon_' + repo.id).is(".hover_icon"), __.equalTo(false));
        assert.assertThat('aui-icon removed', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-icon"), __.equalTo(false));
        assert.assertThat('aui-icon-small removed', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-icon-small"), __.equalTo(false));
        assert.assertThat('aui-iconfont-refresh-small removed', this.fixture.find('#syncrepoicon_' + repo.id).is(".aui-iconfont-refresh-small"), __.equalTo(false));
    });

    QUnit.test('Sync in progress Finished with no error', function (assert, update_sync_status) {
        var idNum = 1;
        var finishedTime = 1451606400;
        var startTime = finishedTime - 1000;
        var startSyncDateTime = new Date(startTime);
        var errorText = "errorText";
        var errorTitle = "errorTitle";
        var changeSetCount = 1;
        var pullRequestActivityCount = 2;
        var jiraCount = 3;

        var repo = {
            id: idNum,
            sync: {
                finished: false,
                startTime: startTime,
                finishTime: finishedTime,
                changesetCount: changeSetCount,
                pullRequestActivityCount: pullRequestActivityCount,
                jiraCount: jiraCount
            }
        };

        update_sync_status(repo, sinon.spy());

        assert.assertThat('In progress sync has expected title', this.fixture.find('#syncrepoicon_' + repo.id).attr('title'),
            __.equalTo("Synchronizing... (started at " + startSyncDateTime.toDateString() + " " + startSyncDateTime.toLocaleTimeString() + ")"));

        assert.assertThat('template should be called with expected arguments', templateSpy.getCall(0).args[0], __.equalTo({
            "changesetCount": changeSetCount,
            "pullRequestCount": pullRequestActivityCount,
            "issueCount": jiraCount
        }));

        assert.assertThat('sync status div should contain template contents',
            this.fixture.find('#sync_status_message_' + repo.id).html(), __.equalTo("template contents"));


    });

    QUnit.test('No sync object shows last activity', function (assert, update_sync_status) {
        var idNum = 1;
        var lastActivityDate = 1451606400;
        var lastActivityDateString = new Date(1451606400).toDateString()

        var repo = {
            id: idNum,
            lastActivityDate: lastActivityDate
        };


        update_sync_status(repo, sinon.spy());

        assert.assertThat('sync status div should contain last activity date', this.fixture.find('#sync_status_message_' + repo.id).html(), __.equalTo(lastActivityDateString));

    });

    function containsHoverIconClasses(assert, __, fixture, repo_id) {
        assert.assertThat('hover_icon was added', fixture.find('#syncrepoicon_' + repo_id).is(".hover_icon"), __.equalTo(true));
        assert.assertThat('aui-icon was added', fixture.find('#syncrepoicon_' + repo_id).is(".aui-icon"), __.equalTo(true));
        assert.assertThat('aui-icon-small was added', fixture.find('#syncrepoicon_' + repo_id).is(".aui-icon-small"), __.equalTo(true));
        assert.assertThat('aui-iconfont-refresh-small was added', fixture.find('#syncrepoicon_' + repo_id).is(".aui-iconfont-refresh-small"), __.equalTo(true));
    }

});