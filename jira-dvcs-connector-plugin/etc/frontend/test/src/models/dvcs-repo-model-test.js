define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'fusion/test/backbone',
    'fusion/test/jquery'
], function (QUnit,
             __,
             Backbone,
             $) {

    QUnit.module('jira-dvcs-connector/bitbucket/models/dvcs-repo-model', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/models/dvcs-repo-model',
            jquery: 'fusion/test/jquery',
            backbone: 'fusion/test/backbone'
        },
        mocks: {
            window: QUnit.moduleMock('jira-dvcs-connector/util/window', () => new Object({BASE_URL: 'test_base_url'})),
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', () => Backbone),
            jquery: QUnit.moduleMock('jquery', () => $)
        },

        beforeEach: function (asserts, RepoModel) {
            // create instance of model
            this.model = new RepoModel({id: 'repo-id'});
            this.listener = sinon.spy();
            this.model.on('change:linked', this.listener);

            // register listener on call model changed
            var errorSpy = this.errorSpy = sinon.spy();
            this.ajaxStub = sinon.stub($, 'ajax', () => new Object({error: errorSpy}));
            asserts.assertThat('model should not be doing anything', this.model.isBusy(), __.equalTo(false));

        },
        afterEach: function () {
            // restore stub
            $.ajax.restore();
        }
    });

    QUnit.test('Should call rest-endpoint to enable repo', function (asserts) {

        this.model.enable();
        asserts.assertThat('model should be working during ajax call', this.model.isBusy(), __.equalTo(true));

        asserts.assertThat('Should perform ajax call', this.ajaxStub.callCount, __.equalTo(1));
        // assert ajax arguments
        var ajaxRequest = this.ajaxStub.firstCall.args[0];
        assertAjaxRequest(asserts, ajaxRequest, true);

        assertErrorSpy(asserts, this.errorSpy);

        // call success callback
        var fakeRegistration = {repository: {smartcommitsEnabled: "true"}};

        ajaxRequest.success(fakeRegistration);

        asserts.assertThat('model should not be doing anything after ajax request returned', this.model.isBusy(), __.equalTo(false));
        asserts.assertThat('model smartCommitsEnabled should reflect state in ajax response', this.model.getSmartcommitsEnabled(), __.equalTo("true"));
        assertListener(asserts, this.listener, true, fakeRegistration);
    });

    QUnit.test('Should handle error when trying to disable repo', function (asserts) {

        this.model.disable();
        asserts.assertThat('model should be working during ajax call', this.model.isBusy(), __.equalTo(true));

        // assert ajax arguments
        var ajaxRequest = this.ajaxStub.firstCall.args[0];
        assertAjaxRequest(asserts, ajaxRequest, false);

        assertErrorSpy(asserts, this.errorSpy);
        // call error
        var err = {err: 'error object'};
        this.errorSpy.firstCall.args[0](err);

        asserts.assertThat('model should not be doing anything after ajax request returned', this.model.isBusy(), __.equalTo(false));
        assertListener(asserts, this.listener, false, undefined);
    });

    function assertAjaxRequest(asserts, ajaxRequest, enabled) {
        asserts.assertThat('ajax request should match rest end-point', ajaxRequest, __.hasProperties({
            url: 'test_base_url/rest/bitbucket/1.0/repo/repo-id/autolink',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                payload: enabled
            }),
            success: __.instanceOf(Function)
        }));
    }

    function assertErrorSpy(asserts, errorSpy) {
        // assert ajax.err is called with function
        asserts.assertThat('Should register callback for ajax error', errorSpy.callCount, __.equalTo(1));
        asserts.assertThat('Should register callback for ajax error', errorSpy.firstCall.args[0], __.instanceOf(Function));

    }

    function assertListener(asserts, listener, linked, registration) {
        asserts.assertThat('listener should be called on linked value change', listener.callCount, __.equalTo(1));
        let model = listener.firstCall.args[0];
        asserts.assertThat('repo linked should be changed to ' + linked, model.isEnabled(), __.equalTo(linked));
        asserts.assertThat('repo should store last registration result' + linked, model.getLastRegistration(), __.equalTo(registration));
    }
});
