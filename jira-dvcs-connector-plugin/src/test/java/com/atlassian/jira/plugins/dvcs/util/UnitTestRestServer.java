package com.atlassian.jira.plugins.dvcs.util;

import com.atlassian.jira.plugins.dvcs.rest.ExceptionHandler;
import com.jayway.restassured.RestAssured;
import org.jboss.resteasy.plugins.server.embedded.EmbeddedJaxrsServer;
import org.jboss.resteasy.plugins.server.embedded.SecurityDomain;
import org.jboss.resteasy.plugins.server.tjws.TJWSEmbeddedJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Provides embedded jaxrs compliant in-memory server that can be used to test rest resource endpoint classes.
 * <p>
 * Intended usage is for testing REST endpoints exposed by DVCS connector within REST server stack, but in isolation
 * from components the endpoint (resource) depends on (i.e. classes from service layer).
 * <p>
 * It sets up the in-memory server such that the REST server layer configuration is similar to how
 * the Jersey server is set up when the plugin is running in JIRA (i.e. configuring relevant Providers
 * such as Exception mapper etc.). It also encapsulates some of the boilerplate code related to server
 * setup such that it does not have to be repeated in every test class. It sets configures RestAssured such that
 * when using RestAssured to invoke the REST endpoints during test, relative URL can be used instead of fully
 * qualified URL (i.e. /organization/1/enable, instead of http://localhost:9999/organization/1/enable).
 * <p>
 * Usage:
 * <ul>
 * <li>In your @Before method, create new instance of this class, invoke {@link #addResource(Object)} providing</li>
 * the REST resource object under test as argument, and invoke {@link #start()}</li>
 * <li>In your @After method, invoke {@link #stop()}</li>
 * </ul>
 * <p>
 * See examples in {@link com.atlassian.jira.plugins.dvcs.rest.external.v1.OrganizationResourceTest}
 */
public class UnitTestRestServer implements EmbeddedJaxrsServer {
    private static final String URL_BASE = "http://localhost";
    private TJWSEmbeddedJaxrsServer server = new TJWSEmbeddedJaxrsServer();

    public void addResource(final Object resource) {
        server.getDeployment().getResources().add(resource);
    }

    @Override
    public void setRootResourcePath(final String rootResourcePath) {
        server.setRootResourcePath(rootResourcePath);
    }

    @Override
    public void start() {
        final int port = getRandomAvailablePort();
        final String url = URL_BASE + ":" + port;
        server.setPort(port);
        server.getDeployment().getActualProviderClasses().add(ExceptionHandler.class);
        server.start();
        RestAssured.baseURI = url;
    }

    @Override
    public void stop() {
        server.stop();
    }

    @Override
    public ResteasyDeployment getDeployment() {
        return server.getDeployment();
    }

    @Override
    public void setDeployment(final ResteasyDeployment deployment) {
        server.setDeployment(deployment);
    }

    @Override
    public void setSecurityDomain(final SecurityDomain sc) {
        server.setSecurityDomain(sc);
    }

    private int getRandomAvailablePort() {
        try {
            final ServerSocket s = new ServerSocket(0);
            s.setReuseAddress(true);
            final int port = s.getLocalPort();
            s.close();
            return port;
        } catch (IOException e) {
            throw new RuntimeException("Failed to allocate free port", e);
        }
    }
}
