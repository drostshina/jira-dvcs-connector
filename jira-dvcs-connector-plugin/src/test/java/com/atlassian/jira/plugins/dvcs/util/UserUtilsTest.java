package com.atlassian.jira.plugins.dvcs.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserIdentity;
import com.atlassian.jira.user.util.UserManager;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserUtilsTest {
    private final TestNGMockComponentContainer mockComponentContainer = new TestNGMockComponentContainer(this);

    @Mock
    @AvailableInContainer
    private UserManager userManager;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockComponentContainer.beforeMethod();
    }

    @Test
    public void shouldReturnNullWhenGivenNull() {
        assertThat(UserUtils.from(null), is(nullValue()));
    }

    @Test
    public void shouldReturnApplicationUserWhenGivenOne() {
        final ApplicationUser applicationUser = mock(ApplicationUser.class);

        assertThat(UserUtils.from(applicationUser), is(applicationUser));
    }

    @Test
    public void shouldReturnApplicationUserWhenGivenCrowdUser() {
        // Set up
        final String username = "jbloggs";
        final User user = mock(User.class);
        final UserIdentity userIdentity = UserIdentity.withId(123L).key("bob").andUsername(username);
        when(user.getName()).thenReturn(username);
        when(userManager.getUserIdentityByUsername(username)).thenReturn(Optional.of(userIdentity));

        // Invoke
        final ApplicationUser applicationUser = UserUtils.from(user);

        // Check
        assertThat(applicationUser, instanceOf(ApplicationUser.class));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldBarfWhenGivenNonUserInstance() {
        UserUtils.from("foo");
    }
}