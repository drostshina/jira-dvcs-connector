package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.ondemand.AccountsConfig.BitbucketAccountInfo;
import com.atlassian.jira.plugins.dvcs.ondemand.AccountsConfig.Links;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.create3LOCredential;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class BitbucketAccountsConfigServiceTest {

    @Captor
    private ArgumentCaptor<Organization> organizationCaptor;

    @Mock
    private AccountsConfigProvider configProvider;

    @Mock
    private BitbucketAccountsReloadJobScheduler mockBitbucketAccountsReloadJobScheduler;

    @Mock
    private OrganizationService organizationService;

    private BitbucketAccountsConfigService testedService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);

        testedService = new BitbucketAccountsConfigService(configProvider, organizationService,
                mockBitbucketAccountsReloadJobScheduler);

        when(configProvider.supportsIntegratedAccounts()).thenReturn(true);
    }

    @Test
    public void testAddNewAccountWithSuccess() {
        final AccountsConfig correctConfig = createCorrectConfig();
        when(configProvider.provideConfiguration()).thenReturn(correctConfig);
        when(organizationService.findIntegratedAccount()).thenReturn(null);
        when(organizationService.getByHostAndName(eq("https://bitbucket.org"), eq("A"))).thenReturn(null);

        testedService.reload();

        verify(organizationService).save(organizationCaptor.capture());

        final Organization savedOrg = organizationCaptor.getValue();

        assertThat(savedOrg.getName()).isEqualTo("A");

        assertThat(savedOrg.getCredential()).isExactlyInstanceOf(TwoLeggedOAuthCredential.class);

        final TwoLeggedOAuthCredential credential = (TwoLeggedOAuthCredential) savedOrg.getCredential();
        assertThat(credential.getKey()).isEqualTo("K");
        assertThat(credential.getSecret()).isEqualTo("S");
    }

    @Test
    public void testAddNewAccountEmptyConfig() {
        when(configProvider.provideConfiguration()).thenReturn(null);
        when(organizationService.findIntegratedAccount()).thenReturn(null);

        testedService.reload();

        verify(organizationService, times(0)).save(organizationCaptor.capture());
    }

    @Test
    public void testUpdateAccountEmptyConfig() {
        when(configProvider.provideConfiguration()).thenReturn(null);

        final Organization existingAccount = createSampleAccount("A", "B", "S", "token");
        when(organizationService.findIntegratedAccount()).thenReturn(existingAccount);

        testedService.reload();

        verify(organizationService, times(0)).save(organizationCaptor.capture());
        verify(organizationService).remove(eq(5));
    }

    @Test
    public void testUpdateAccountCredentialsWithSuccess() {
        final AccountsConfig correctConfig = createCorrectConfig();
        when(configProvider.provideConfiguration()).thenReturn(correctConfig);

        final Organization existingAccount = createSampleAccount("A", "B", "S", "token");
        when(organizationService.findIntegratedAccount()).thenReturn(existingAccount);
        when(organizationService.getByHostAndName(eq("https://bitbucket.org"), eq("A"))).thenReturn(null);

        testedService.reload();

        verify(organizationService).findIntegratedAccount();
        verify(organizationService).getByHostAndName("https://bitbucket.org", "A");
        verifyNoMoreInteractions(organizationService);
    }

    @Test
    public void testUpdateAccountCredentialsEmptyConfig_ShouldRemoveIntegratedAccount() {
        when(configProvider.provideConfiguration()).thenReturn(null);

        final Organization existingAccount = createSampleAccount("A", "B", "S", "token");
        when(organizationService.findIntegratedAccount()).thenReturn(existingAccount);

        when(organizationService.getByHostAndName(eq("https://bitbucket.org"), eq("A"))).thenReturn(null);

        testedService.reload();

        verify(organizationService).remove(eq(5));
    }

    @Test
    public void testAddNewAccountWithSuccess_UserAddedAccountExists() {
        final AccountsConfig config = createCorrectConfig();
        when(configProvider.provideConfiguration()).thenReturn(config);

        when(organizationService.findIntegratedAccount()).thenReturn(null);

        final Organization userAddedAccount = createSampleAccount("A", "key", "secret", "token");
        when(organizationService.getByHostAndName(eq("https://bitbucket.org"), eq("A"))).thenReturn(userAddedAccount);

        testedService.reload();

        verify(organizationService).updateCredentials(userAddedAccount.getId(), CredentialFactory.create2LOCredential("K", "S"));
    }

    private Organization createSampleAccount(final String name, final String key, final String secret, final String token) {
        final Organization org = new Organization();
        org.setId(5);
        org.setName(name);
        org.setCredential(create3LOCredential(key, secret, token));
        return org;
    }

    private AccountsConfig createCorrectConfig() {
        return createConfig("A", "K", "S");
    }

    private AccountsConfig createConfig(final String account, final String key, final String secret) {
        final AccountsConfig accountsConfig = new AccountsConfig();
        final List<Links> sysadminApplicationLinks = new ArrayList<>();
        final Links links = new Links();
        final List<BitbucketAccountInfo> bbLinks = new ArrayList<>();
        final BitbucketAccountInfo bbAccount = new BitbucketAccountInfo();
        bbAccount.setAccount(account);
        bbAccount.setKey(key);
        bbAccount.setSecret(secret);
        bbLinks.add(bbAccount);
        links.setBitbucket(bbLinks);
        sysadminApplicationLinks.add(links);
        accountsConfig.setSysadminApplicationLinks(sysadminApplicationLinks);
        return accountsConfig;
    }
}
