package it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule;

import com.atlassian.fusion.aci.test.ACITestClient;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.util.TestRule;

import java.util.Optional;

import static com.atlassian.fusion.aci.api.service.ACIRegistrationService.DESCRIPTOR_KEY;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Removes the OAuth consumer module from addon descriptor
 */
public class RemoveOAuthClientKeyFromDescriptorRule extends TestRule {
    private final String jiraUrl;

    public RemoveOAuthClientKeyFromDescriptorRule(final String jiraUrl) {
        this.jiraUrl = checkNotNull(jiraUrl);
    }

    @Override
    protected void doApply() throws Exception {
        final ACITestClient testClient = new ACITestClient(jiraUrl, ImmutableMap.of());
        final Optional<String> maybeDescriptor = testClient.getDescriptor(BITBUCKET_CONNECTOR_APPLICATION_ID);

        final String descriptorText = maybeDescriptor.get();

        final JsonParser jsonParser = new JsonParser();
        JsonObject descriptorJson = jsonParser.parse(descriptorText).getAsJsonObject();

        if (descriptorJson.getAsJsonObject("modules").has("oauthConsumer")) {
            descriptorJson.getAsJsonObject("modules").remove("oauthConsumer");

            // reset this so that we don't double append the applinks id
            descriptorJson.getAsJsonObject().addProperty(DESCRIPTOR_KEY, BITBUCKET_CONNECTOR_APPLICATION_ID);
            testClient.registerApplication(BITBUCKET_CONNECTOR_APPLICATION_ID, descriptorJson.toString());
        }
    }
}
