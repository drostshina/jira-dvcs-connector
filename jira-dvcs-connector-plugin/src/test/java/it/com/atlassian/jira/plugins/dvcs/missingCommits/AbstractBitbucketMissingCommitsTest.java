package it.com.atlassian.jira.plugins.dvcs.missingCommits;

import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.BitBucketConfigureOrganizationsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BitbucketRequestException;

import static it.util.TestUtils.toOAuth;

/**
 * Base Bitbucket class for GIT and Mercurial tests.
 */
public abstract class AbstractBitbucketMissingCommitsTest
        extends AbstractMissingCommitsTest<BitBucketConfigureOrganizationsPage> {
    protected static final BitbucketRemoteClient BITBUCKET_CLIENT = new BitbucketRemoteClient(DVCS_REPO_OWNER, DVCS_REPO_PASSWORD);
    protected static final BitbucketTestedProduct BITBUCKET = new BitbucketTestedProduct(JIRA.getTester());

    @Override
    void removeOldDvcsRepository() {
        removeRepository(MISSING_COMMITS_REPOSITORY_NAME_PREFIX);
    }

    @Override
    void removeRemoteDvcsRepository() {
        removeRepository(getMissingCommitsRepositoryName());

        BITBUCKET_CLIENT.getRepositoriesRest()
                .getAllRepositories(DVCS_REPO_OWNER)
                .stream()
                .filter(repository -> timestampNameTestResource.isExpired(repository.getName()))
                .forEach(repository -> removeRepository(repository.getName()));
    }

    @Override
    OAuth loginToDvcsAndGetJiraOAuthCredentials() {
        BITBUCKET.login(DVCS_REPO_OWNER, DVCS_REPO_PASSWORD);
        return toOAuth(BITBUCKET_CLIENT.getConsumerRemoteRestpoint().createConsumer(DVCS_REPO_OWNER));
    }

    @Override
    void removeOAuth() {
        try {
            if (oAuth != null) {
                BITBUCKET_CLIENT.getConsumerRemoteRestpoint().deleteConsumer(DVCS_REPO_OWNER, oAuth.applicationId);
            }
        } finally {
            BITBUCKET.logout();
        }
    }

    @Override
    protected Class<BitBucketConfigureOrganizationsPage> getConfigureOrganizationsPageClass() {
        return BitBucketConfigureOrganizationsPage.class;
    }

    @Override
    protected Account.AccountType getAccountType() {
        return Account.AccountType.BITBUCKET;
    }

    private void removeRepository(String name) {
        try {
            BITBUCKET_CLIENT.getRepositoriesRest().removeRepository(DVCS_REPO_OWNER, name);
        } catch (BitbucketRequestException.NotFound_404 ignored) {
        } // the repo does not exist
    }
}
