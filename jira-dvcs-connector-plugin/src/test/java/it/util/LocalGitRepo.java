package it.util;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.StoredConfig;

import java.io.File;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.testng.Assert.assertNotNull;

/**
 * <p>Utility class with helper methods for making it easier to work with JGit based
 * local GIT repositories for testing purposes.
 * <p>This class is currently just a set of static utility methods that operate
 * on top of JGit objects. We can change this later into instantiable class that
 * encapsulates the JGit objects and adds our functionality on top of it. Leaving
 * as static utility helper for now.
 */
public class LocalGitRepo {
    /**
     * <p>Creates new GIT repo on local filesystem. This GIT repo will reside in randomly named
     * directory in the System defined default temporary directory.
     *
     * @return {@link Git} object representing the local Git repo.
     */
    public static Git createLocalTempGitRepo() {
        final File gitWorkingDir = Files.createTempDir();

        checkNotNull(gitWorkingDir);
        final InitCommand initCommand = Git.init();
        initCommand.setDirectory(gitWorkingDir);
        try {
            final Git git = initCommand.call();
            assertNotNull(git);
            return git;
        } catch (GitAPIException e) {
            throw new RuntimeException("Failed to initialize temp git repo. Path: "
                    + gitWorkingDir.getAbsolutePath(), e);
        }
    }

    /**
     * Removes local Git repo from filesystem.
     *
     * @param localGitRepo to remove
     */
    public static void removeLocalGitRepo(Git localGitRepo) {
        checkNotNull(localGitRepo);
        localGitRepo.getRepository().close();
        try {
            FileUtils.deleteDirectory(localGitRepo.getRepository().getDirectory());
        } catch (IOException e) {
            throw new RuntimeException("Failed to delete directory containing local git repo.", e);
        }
    }

    /**
     * Configures provided 'gitRepo' such that it's 'origin' is pointing to provided Bitbucket repo.
     *
     * @param gitRepo     to configure
     * @param accountName of target Bitbucket repo
     * @param repoSlug    of target Bitbucket repo
     * @return instance of {@link Git} representing the Git repo that was configured
     */
    public static Git configureRemoteOriginToBitbucket(Git gitRepo, final String accountName, final String repoSlug) {
        final StoredConfig config = gitRepo.getRepository().getConfig();
        final String remoteUrl = BitbucketUtils.createBitbucketRepoRemoteUrl(accountName, repoSlug);
        config.setString("remote", "origin", "url", remoteUrl);
        try {
            config.save();
        } catch (IOException e) {
            throw new RuntimeException("Failed to save temp GIT repo config while attempting"
                    + " to configure remote origin.", e);
        }
        return gitRepo;
    }
}
