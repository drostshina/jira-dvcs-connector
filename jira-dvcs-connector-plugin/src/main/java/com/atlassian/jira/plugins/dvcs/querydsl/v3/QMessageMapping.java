package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.PrimaryKey;


public class QMessageMapping extends EnhancedRelationalPathBase<QMessageMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_MESSAGE";
    private static final long serialVersionUID = 6356640968388253507L;

    public final NumberPath<Integer> ID = createNumber("ID", Integer.class);
    public final StringPath ADDRESS = createString("ADDRESS");
    public final StringPath PAYLOAD_TYPE = createString("PAYLOAD_TYPE");
    public final StringPath PAYLOAD = createString("PAYLOAD");
    public final NumberPath<Integer> PRIORITY = createNumber("PRIORITY", Integer.class);

    public final PrimaryKey<QMessageMapping> MESSAGE_PK = createPrimaryKey(ID);
    public final ForeignKey<QMessageTagMapping> MESSAGE_TAGS_FK = createInvForeignKey(ID, "MESSAGE_ID");
    public final ForeignKey<QMessageQueueItemMapping> MESSAGE_QUEUE_ITEM_FK = createInvForeignKey(ID, "MESSAGE_ID");

    public QMessageMapping() {
        super(QMessageMapping.class, AO_TABLE_NAME);
    }

}
