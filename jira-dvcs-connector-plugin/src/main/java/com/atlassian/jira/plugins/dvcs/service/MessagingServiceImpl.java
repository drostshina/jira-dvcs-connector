package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.dao.MessageDao;
import com.atlassian.jira.plugins.dvcs.dao.MessageQueueItemDao;
import com.atlassian.jira.plugins.dvcs.dao.SyncAuditLogDao;
import com.atlassian.jira.plugins.dvcs.event.CarefulEventService;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.DiscardReason;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;
import com.atlassian.jira.plugins.dvcs.model.MessageQueueItem;
import com.atlassian.jira.plugins.dvcs.model.MessageState;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.HasProgress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddressService;
import com.atlassian.jira.plugins.dvcs.service.message.MessageConsumer;
import com.atlassian.jira.plugins.dvcs.service.message.MessagePayloadSerializer;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Stream;

import static com.atlassian.jira.plugins.dvcs.model.MessageState.WAITING_FOR_RETRY;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Integer.parseInt;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.startsWith;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

/**
 * A {@link MessagingService} implementation.
 *
 * @author Stanislav Dvorscak
 */
@Component
public class MessagingServiceImpl implements MessagingService {

    @VisibleForTesting
    static final String SYNCHRONIZATION_REPO_TAG_PREFIX = "synchronization-repository-";

    private static final String SYNCHRONIZATION_AUDIT_TAG_PREFIX = "audit-id-";

    /**
     * Logger for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(MessagingServiceImpl.class);
    /**
     * Maps between {@link MessageConsumer#getAddress()} and appropriate {@link MessageConsumer consumers}.
     */
    private final ConcurrentMap<String, List<MessageConsumer<?>>> addressToMessageConsumer = new ConcurrentHashMap<>();
    /**
     * Contains all tags which are currently paused.
     */
    private final Set<String> pausedTags = new CopyOnWriteArraySet<>();
    /**
     * Maps between {@link MessagePayloadSerializer#getPayloadType()} and appropriate {@link MessagePayloadSerializer
     * serializer}.
     */
    private final Map<Class<?>, MessagePayloadSerializer<?>> payloadTypeToPayloadSerializer = new ConcurrentHashMap<>();
    /**
     * Maps between {@link MessageConsumer#getQueue()} and appropriate {@link MessageConsumer}.
     */
    private final ConcurrentMap<String, MessageConsumer<?>> queueToMessageConsumer = new ConcurrentHashMap<>();
    @Resource
    protected ChangesetService changesetService;
    @Resource
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    private MessageConsumer<?>[] consumers;
    @Resource
    private CarefulEventService eventService;
    @Resource
    private HttpClientProvider httpClientProvider;
    @Resource
    private LinkerService linkerService;
    @Resource
    private MessageAddressService messageAddressService;

    @Resource
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    private MessageConsumer<?>[] messageConsumers;

    @Resource
    private MessageDao messageDao;

    @Resource
    private MessageExecutor messageExecutor;

    @Resource(name = "MessageQueueItemDaoQueryDsl")
    private MessageQueueItemDao messageQueueItemDao;

    @Resource
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    private MessagePayloadSerializer<?>[] payloadSerializers;

    @Resource
    private RepositoryService repositoryService;

    @Resource
    private SyncAuditLogDao syncAudit;

    @Resource
    private Synchronizer synchronizer;

    /**
     * Indicates whether {@code progress} contains any errors.
     *
     * @param progress the progress to check
     * @return see above
     */
    private static boolean hasErrors(@Nullable final Progress progress) {
        return progress != null && progress.getError() != null;
    }

    /**
     * Initializes bean.
     */
    @PostConstruct
    public void init() {
        for (MessageConsumer<?> messageConsumer : messageConsumers) {
            queueToMessageConsumer.putIfAbsent(messageConsumer.getQueue(), messageConsumer);
            List<MessageConsumer<?>> byAddress = addressToMessageConsumer.get(messageConsumer.getAddress().getId());
            if (byAddress == null) {
                addressToMessageConsumer.putIfAbsent(messageConsumer.getAddress().getId(),
                        byAddress = new CopyOnWriteArrayList<>());
            }
            byAddress.add(messageConsumer);
        }

        for (MessagePayloadSerializer<?> payloadSerializer : payloadSerializers) {
            payloadTypeToPayloadSerializer.put(payloadSerializer.getPayloadType(), payloadSerializer);
        }
    }

    /**
     * Marks failed all messages, which are in state running
     */
    private void initRunningToFail() {
        log.debug("Setting messages in running state to fail");
        messageQueueItemDao.getByState(MessageState.RUNNING, queueItemId -> {
            MessageQueueItem item = messageQueueItemDao.getQueueItemById(queueItemId)
                    .orElseThrow(IllegalStateException::new);

            @SuppressWarnings("unchecked")
            final MessageConsumer<HasProgress> consumer =
                    (MessageConsumer<HasProgress>) queueToMessageConsumer.get(item.getQueue());

            fail(consumer, item.getMessage(),
                    new RuntimeException("Synchronization has been interrupted (probably plugin un/re/install)."));
        });
    }

    /**
     * Restart consumers.
     */
    private void restartConsumers() {
        log.debug("Restarting message consumers");
        Set<String> addresses = new HashSet<>();
        for (MessageConsumer<?> consumer : consumers) {
            addresses.add(consumer.getAddress().getId());
        }
        addresses.forEach(messageExecutor::notify);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <P extends HasProgress> void publish(final MessageAddress<P> address, final P payload, final String... tags) {
        publish(address, payload, MessagingService.DEFAULT_PRIORITY, tags);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <P extends HasProgress> void publish(
            final MessageAddress<P> address, final P payload, final int priority, final String... tags) {
        MessageState state = MessageState.PENDING;
        for (String tag : tags) {
            if (pausedTags.contains(tag)) {
                state = MessageState.SLEEPING;
                break;
            }
        }

        @SuppressWarnings("unchecked")
        MessagePayloadSerializer<P> payloadSerializer =
                (MessagePayloadSerializer<P>) payloadTypeToPayloadSerializer.get(payload.getClass());

        final Message<P> message = new Message<>();
        message.setAddress(address);
        message.setPayload(payloadSerializer.serialize(payload));
        message.setPayloadType(address.getPayloadType());
        message.setTags(tags);
        message.setPriority(priority);

        createMessage(message, state, tags);

        messageExecutor.notify(address.getId());
    }

    protected <P extends HasProgress> void createMessage(Message<P> message, MessageState state, String... tags) {
        Message createdMessage = messageDao.create(toMessageMap(message), tags);

        @SuppressWarnings({"rawtypes", "unchecked"})
        List<MessageConsumer<P>> byAddress = (List) addressToMessageConsumer.get(message.getAddress().getId());
        for (MessageConsumer<P> consumer : byAddress) {
            messageQueueItemDao.create(messageQueueItemToMap(createdMessage.getId(), consumer.getQueue(), state, null));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pause(Repository repository) {
        String tag = getTagForSynchronization(repository);
        pausedTags.add(tag);
        final Set<Integer> syncAudits = new LinkedHashSet<>();
        messageDao.getByTag(tag, messageId -> {
            Stream.of(messageQueueItemDao.getByMessageId(messageId))
                    .filter(queueItem -> !MessageState.RUNNING.name().equals(queueItem.getState()))
                    .forEach(messageQueueItem -> {
                        messageQueueItem.setState(MessageState.SLEEPING.name());
                        messageQueueItemDao.save(messageQueueItem);
                    });

            int syncAuditId = getSynchronizationAuditIdFromTags(messageDao.getTags(messageId));
            if (syncAuditId != 0) {
                syncAudits.add(syncAuditId);
            }
        });
        syncAudits.forEach(syncAudit::pause);
    }

    @Override
    public <P extends HasProgress> P deserializePayload(Message<P> message) {
        @SuppressWarnings("unchecked")
        final MessagePayloadSerializer<P> payloadSerializer =
                (MessagePayloadSerializer<P>) payloadTypeToPayloadSerializer.get(message.getPayloadType());
        return payloadSerializer.deserialize(message);
    }

    @Override
    public void resume(Repository repository) {
        String tag = getTagForSynchronization(repository);
        pausedTags.remove(tag);
        final Set<String> addresses = new HashSet<>();
        final Set<Integer> syncAudits = new HashSet<>();

        messageQueueItemDao.getByTagAndState(tag, MessageState.SLEEPING, queueItemId -> {
            MessageQueueItem item = messageQueueItemDao.getQueueItemById(queueItemId)
                    .orElseThrow(IllegalStateException::new);

            item.setState(MessageState.PENDING.name());
            messageQueueItemDao.save(item);
            addresses.add(item.getMessage().getAddress().getId());

            int syncAuditId = getSynchronizationAuditIdFromTags(item.getMessage().getTags());
            if (syncAuditId != 0) {
                syncAudits.add(syncAuditId);
            }
        });

        addresses.forEach(messageExecutor::notify);
        syncAudits.forEach(syncAudit::resume);
    }

    @Override
    public void retry(final String tag, final int auditId) {
        final Set<String> addresses = new HashSet<>();
        messageQueueItemDao.getByTagAndState(tag, WAITING_FOR_RETRY, queueItemId -> {
            MessageQueueItem item = messageQueueItemDao.getQueueItemById(queueItemId)
                    .orElseThrow(IllegalStateException::new);
            updateSyncAuditId(auditId, item);
            addresses.add(item.getMessage().getAddress().getId());
            item.setState(MessageState.PENDING.name());
            messageQueueItemDao.save(item);
        });

        addresses.forEach(messageExecutor::notify);
    }

    private void updateSyncAuditId(final int auditId, MessageQueueItem queueItem) {
        String newSyncAuditIdLog = getTagForAuditSynchronization(auditId);

        Stream.of(queueItem.getMessage().getTags())
                .filter(tag -> tag.startsWith(SYNCHRONIZATION_AUDIT_TAG_PREFIX))
                .forEach(tag -> messageDao.deleteTag(queueItem.getMessage(), tag));

        messageDao.createTag(queueItem.getMessage(), newSyncAuditIdLog);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel(Repository repository) {
        String tag = getTagForSynchronization(repository);
        messageDao.getByTag(tag, messageId -> {
            Message fullMessage = messageDao.getById(messageId.getId());
            MessageQueueItem[] queueItems = messageQueueItemDao.getByMessageId(messageId);
            if (queueItems != null) {
                Stream.of(queueItems).forEach(messageQueueItemDao::delete);
            }
            messageDao.delete(fullMessage);
        });
    }

    @Override
    public <P extends HasProgress> void running(final MessageConsumer<P> consumer, final Message<P> message) {
        messageQueueItemDao.getByQueueAndMessage(consumer.getQueue(), message.getId()).ifPresent(queueItem -> {
            queueItem.setState(MessageState.RUNNING.name());
            messageQueueItemDao.save(queueItem);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <P extends HasProgress> void ok(MessageConsumer<P> consumer, Message<P> message) {
        messageQueueItemDao.getByQueueAndMessage(consumer.getQueue(), message.getId())
                .ifPresent(messageQueueItemDao::delete);

        if (messageQueueItemDao.getByMessageId(new MessageId(message)).length == 0) {
            messageDao.delete(message);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <P extends HasProgress> void fail(MessageConsumer<P> consumer, Message<P> message, Throwable t) {
        messageQueueItemDao.getByQueueAndMessage(consumer.getQueue(), message.getId()).ifPresent(queueItem -> {
            queueItem.setRetryCount(queueItem.getRetryCount() + 1);
            queueItem.setState(WAITING_FOR_RETRY.name());
            messageQueueItemDao.save(queueItem);
            syncAudit.setException(getSynchronizationAuditIdFromTags(message.getTags()), t, false);
        });
    }

    @Override
    public <P extends HasProgress> void discard(
            final MessageConsumer<P> consumer, final Message<P> message, final DiscardReason discardReason) {
        messageQueueItemDao.getByQueueAndMessage(consumer.getQueue(), message.getId()).ifPresent(queueItem -> {
                    queueItem.setState(MessageState.DISCARDED.name());
                    queueItem.setStateInfo(discardReason.name());
                    messageQueueItemDao.save(queueItem);
                }
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <P extends HasProgress> Message<P> getNextMessageForConsuming(MessageConsumer<P> consumer, String address) {
        return messageQueueItemDao.getNextItemForProcessing(consumer.getQueue(), address);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getQueuedCount(String tag) {
        return messageDao.getMessagesForConsumingCount(tag);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public <P extends HasProgress> MessageAddress<P> get(final Class<P> payloadType, final String id) {
        return messageAddressService.get(payloadType, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTagForSynchronization(Repository repository) {
        return SYNCHRONIZATION_REPO_TAG_PREFIX + repository.getId();
    }

    @Override
    public String getTagForAuditSynchronization(int id) {
        return SYNCHRONIZATION_AUDIT_TAG_PREFIX + id;
    }

    @Override
    public int getSynchronizationAuditIdFromTags(final String[] tags) {
        for (final String tag : tags) {
            final String syncIdString = substringAfter(tag, SYNCHRONIZATION_AUDIT_TAG_PREFIX);
            if (isNotBlank(syncIdString)) {
                try {
                    return parseInt(syncIdString);
                } catch (final NumberFormatException e) {
                    log.error("Synchronization audit id tag has invalid format. Tag was: '{}'", tag);
                }
            }
        }
        log.warn("No synchronization audit id tag found in {}; {}",
                Arrays.toString(tags), getStackTrace(new Exception("Dummy exception")));
        return 0;
    }

    @Override
    public <P extends HasProgress> Repository getRepositoryFromMessage(Message<P> message) {
        for (String tag : message.getTags()) {
            if (startsWith(tag, SYNCHRONIZATION_REPO_TAG_PREFIX)) {
                int repositoryId;
                try {
                    repositoryId = parseInt(tag.substring(SYNCHRONIZATION_REPO_TAG_PREFIX.length()));
                    return repositoryService.get(repositoryId);

                } catch (NumberFormatException e) {
                    log.warn("Get repo ID from message: " + e.getMessage());
                }
            }
        }

        log.warn("Can't get repository ID from tags for message with ID {}", message.getId());
        return null;
    }

    /**
     * Re-maps provided {@link Message} to parameters.
     *
     * @param source of mapping
     * @return mapped entity
     */
    private <P extends HasProgress> Map<String, Object> toMessageMap(Message<P> source) {
        final Map<String, Object> result = new HashMap<>();

        result.put(MessageMapping.ADDRESS, source.getAddress().getId());
        result.put(MessageMapping.PRIORITY, source.getPriority());
        result.put(MessageMapping.PAYLOAD_TYPE, source.getPayloadType().getCanonicalName());
        result.put(MessageMapping.PAYLOAD, source.getPayload());

        return result;
    }

    /**
     * Re-maps provided data to {@link MessageQueueItemMapping} parameters.
     *
     * @param messageId {@link Message#getId()}
     * @param queue     the queue
     * @param state     the message state
     * @param stateInfo ?
     * @return mapped entity
     */
    private Map<String, Object> messageQueueItemToMap(int messageId, String queue, MessageState state, String stateInfo) {
        checkNotNull(messageId);
        final Map<String, Object> result = new HashMap<>();
        result.put(MessageQueueItemMapping.MESSAGE, messageId);
        result.put(MessageQueueItemMapping.QUEUE, queue);
        result.put(MessageQueueItemMapping.STATE, state.name());
        result.put(MessageQueueItemMapping.STATE_INFO, stateInfo);
        result.put(MessageQueueItemMapping.RETRIES_COUNT, 0);

        return result;
    }

    @Override
    public <P extends HasProgress> void tryEndProgress(
            final Repository repository, final Progress progress, final MessageConsumer<P> consumer, final int auditId) {
        boolean finished = endProgress(repository, progress);
        if (finished) {
            pausedTags.remove(getTagForSynchronization(repository));

            if (auditId > 0) {
                final Date finishDate;
                final Date firstRequestDate;
                final int numRequests;
                final int flightTimeMs;

                if (progress == null) {
                    finishDate = new Date();
                    firstRequestDate = null;
                    numRequests = 0;
                    flightTimeMs = 0;
                } else {
                    finishDate = new Date(progress.getFinishTime());
                    firstRequestDate = progress.getFirstMessageTime();
                    numRequests = progress.getNumRequests();
                    flightTimeMs = progress.getFlightTimeMs();
                }

                syncAudit.finish(auditId, firstRequestDate, numRequests, flightTimeMs, finishDate);
            }
        }
    }

    /**
     * Ends the progress if there are no more messages currently queued for the given repository.
     *
     * @return a boolean indicating whether sync progress was ended
     */
    private boolean endProgress(final Repository repository, final Progress progress) {
        final int queuedCount = getQueuedCount(getTagForSynchronization(repository));
        if (queuedCount == 0) {
            try {
                final boolean hasErrors = hasErrors(progress);
                if (progress != null && !progress.isFinished()) {
                    progress.finish();

                    EnumSet<SynchronizationFlag> flags = progress.getRunAgainFlags();
                    if (flags != null) {
                        progress.setRunAgainFlags(null);
                        // to be sure that soft sync will be run
                        flags.add(SynchronizationFlag.SOFT_SYNC);
                        try {
                            synchronizer.doSync(repository, flags);
                        } catch (SourceControlException.SynchronizationDisabled e) {
                            // ignoring
                        }
                    }
                }

                if (!hasErrors) {
                    eventService.dispatchEvents(repository);
                }

                linkerService.updateConnectLinkerValues(repository.getOrganizationId());

                return true;
            } finally {
                httpClientProvider.closeIdleConnections();
            }
        }

        return false;
    }

    @Override
    public void onStart() {
        initRunningToFail();
        restartConsumers();
    }
}
