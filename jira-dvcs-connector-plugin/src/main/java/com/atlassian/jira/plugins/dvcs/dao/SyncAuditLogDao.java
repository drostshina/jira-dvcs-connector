package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;

import java.util.Date;
import java.util.Set;

public interface SyncAuditLogDao {

    SyncAuditLogMapping newSyncAuditLog(int repoId, Set<SynchronizationFlag> flags, Date startDate);

    void finish(int syncId, Date firstRequestDate, int numRequests, int flightTimeMs, Date finishDate);

    void setException(int syncId, Throwable t, boolean overwriteOld);

    int removeAllForRepo(int repoId);

    SyncAuditLogMapping[] getAll(Integer page);

    SyncAuditLogMapping[] getAllForRepo(int repoId, Integer page);

    void pause(int syncId);

    void resume(int syncId);
}
