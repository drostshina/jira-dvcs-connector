package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.fusion.aci.api.service.exception.ConnectApplicationNotFoundException;
import com.atlassian.fusion.aci.api.service.exception.InstallationNotFoundException;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.AciInstallationServiceAccessor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import com.atlassian.jira.plugins.dvcs.util.DvcsConstants;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;

import static com.atlassian.jira.config.properties.APKeys.JIRA_TITLE;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Scanned
@ParametersAreNonnullByDefault
public class BitbucketPostInstallApprovalAction extends JiraWebActionSupport {
    private static final Logger log = LoggerFactory.getLogger(BitbucketPostInstallApprovalAction.class);

    private static final String REQUIRE_RESOURCE_KEY =
            DvcsConstants.PLUGIN_KEY + ":bitbucket-post-install-approval-resources";
    private static final String APPROVAL_VIEW_NAME = "approval";
    private static final String APPROVAL_ERROR_VIEW_NAME = "approvalError";
    private static final String NON_ADMIN_VIEW_NAME = "nonAdmin";
    private static final String REDIRECT_TO_ADMIN_VIEW_NAME = "redirectToAdminView";
    private static final String UNKNOWN_ERROR_VIEW_NAME = "unknownError";
    private static final String BITBUCKET_ERROR_ACCESS_DENIED = "access_denied";

    private final AciInstallationServiceAccessor aciInstallationServiceAccessor;
    private final ApplicationProperties salApplicationProperties;
    private final com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final BitbucketPostInstallApprovalActionUrlProvider urlProvider;
    private final PageBuilderService pageBuilderService;
    private final OrganizationService organizationService;

    // Query parameter fields
    private boolean isJiraInitiated = false;
    private String principalUuid = "";
    private String accountName = "";
    private String clientKey = "";
    private boolean isApprovalError;
    private boolean isUnknownError;
    private String unknownErrorMessage = "";
    private String bitbucketError = ""; // Error received from Bitbucket
    private String bitbucketErrorDescription = ""; // Error description received from Bitbucket

    // Member variables (i.e. member variables not related to URL query parameters)
    private Installation installation;
    private Organization organization;

    public BitbucketPostInstallApprovalAction(
            @ComponentImport final ApplicationProperties salApplicationProperties,
            @ComponentImport final com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final PageBuilderService pageBuilderService,
            final AciInstallationServiceAccessor aciInstallationServiceAccessor,
            final BitbucketPostInstallApprovalActionUrlProvider urlProvider,
            final OrganizationService organizationService) {
        this.salApplicationProperties = checkNotNull(salApplicationProperties);
        this.jiraApplicationProperties = checkNotNull(jiraApplicationProperties);
        this.jiraAuthenticationContext = checkNotNull(jiraAuthenticationContext);
        this.globalPermissionManager = checkNotNull(globalPermissionManager);
        this.pageBuilderService = checkNotNull(pageBuilderService);
        this.aciInstallationServiceAccessor = checkNotNull(aciInstallationServiceAccessor);
        this.urlProvider = checkNotNull(urlProvider);
        this.organizationService = checkNotNull(organizationService);
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * PrincipalUuid identifying the Org to approve
     */
    public void setPrincipalUuid(final String principalUuid) {
        this.principalUuid = principalUuid;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * ClientKey identifying the Org to approve. This is an alternative to principalUuid
     * in the Jira initiated flow, in which case BB sends us clientKey instead of principalUuid (weird).
     */
    public void setClientKey(final String clientKey) {
        this.clientKey = clientKey;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * Determining whether this is in response to Jira or BB initiated flow. If set to any value
     * it means this is Jira initiated flow.
     */
    public void setJiraInitiated(final String value) {
        this.isJiraInitiated = true;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * Name of the BB Account that is being connected to Jira.
     */
    public void setAccountName(final String accountName) {
        this.accountName = accountName;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * This param is used by this view to show an error in case the Org that is supposed to be approved
     * is not present in Jira anymore.
     */
    public void setApprovalError(final String error) {
        this.isApprovalError = true;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * This param is used by this view to show a generic error message in case of an unexpected error or
     * an error sent to us by Bitbucket, which the user cannot easily recover from.
     */
    public void setUnknownError(final String error) {
        this.isUnknownError = true;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * This param is used by this view to show a generic error message in case of an unexpected error or
     * an error sent to us by Bitbucket, which the user cannot easily recover from.
     */
    public void setUnknownErrorMessage(final String message) {
        this.unknownErrorMessage = message;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * This param represents an error that occured on Bitbucket side before redirecting to Jira.
     */
    public void setError(final String error) {
        this.bitbucketError = error;
    }

    /**
     * Query param setter (used by webwork to inject query param values).
     * This param represents an error description of error that occured on Bitbucket side before redirecting to Jira.
     */
    public void setError_description(final String errorDescription) {
        this.bitbucketErrorDescription = errorDescription;
    }

    @Override
    protected String doExecute() {
        pageBuilderService.assembler().resources().requireWebResource(REQUIRE_RESOURCE_KEY);

        if (!isCurrentUserAdmin()) {
            return NON_ADMIN_VIEW_NAME;
        }

        if (isNotBlank(bitbucketError) || isNotBlank(bitbucketErrorDescription)) {
            if (BITBUCKET_ERROR_ACCESS_DENIED.equals(bitbucketError)) {
                // When user clicks "Cancel" in BB scope approval screen, they get redirected to this page
                // and we want to redirect them straight to DVCS Admin page
                return REDIRECT_TO_ADMIN_VIEW_NAME;
            } else {
                logBitbucketError();
                return UNKNOWN_ERROR_VIEW_NAME;
            }
        }

        checkRequiredQueryParams();

        if (isApprovalError) {
            return APPROVAL_ERROR_VIEW_NAME;
        }
        if (isUnknownError) {
            logUnknownError();
            return UNKNOWN_ERROR_VIEW_NAME;
        }

        try {
            this.installation = getAciInstallation();
        } catch (NotFoundException e) {
            return APPROVAL_ERROR_VIEW_NAME;
        }

        this.organization = organizationService.getByHostAndName(
                installation.getBaseUrl(), installation.getPrincipalUsername());
        if (this.organization == null) {
            log.debug("Organization does not yet exist, however Aci Installation is present. Triggering Org creation "
                    + "based on the Aci Installation found. Principal id: {}", installation.getPrincipalUuid());
            organizationService.createNewOrgBasedOnAciInstallation(installation);
            this.organization = organizationService.getByHostAndName(
                    installation.getBaseUrl(), installation.getPrincipalUsername());

            if (this.organization == null) {
                return APPROVAL_ERROR_VIEW_NAME;
            }
        }

        if (this.organization.getApprovalState() == Organization.ApprovalState.APPROVED) {
            return REDIRECT_TO_ADMIN_VIEW_NAME;
        }

        return APPROVAL_VIEW_NAME;
    }

    private boolean isCurrentUserAdmin() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

    private void logUnknownError() {
        log.error("An error occured while a user was trying to approve an Organization. Error message: {}",
                unknownErrorMessage);
    }

    private void logBitbucketError() {
        log.error("Failed to display BB Connection approval screen due to Bitbucket indicating "
                        + "an error in Jira redirect. Error: {}. Error description: {}",
                bitbucketError,
                bitbucketErrorDescription);
    }

    private void checkRequiredQueryParams() {
        if (isBlank(principalUuid) && isBlank(clientKey)) {
            log.error("We received none of the following query params: principalUuid, clientKey. "
                    + "At least one of them is required for this action.");
            isUnknownError = true;
        }
    }

    private Installation getAciInstallation() {
        if (isNotBlank(principalUuid)) {
            return getAciInstallationByPrincipalUuid(principalUuid);
        } else {
            return getAciInstallationByClientKey(clientKey);
        }
    }

    private Installation getAciInstallationByPrincipalUuid(final String principalUuid) {
        log.debug("Retrieving ACI Installation based on principalUuid: {}", principalUuid);
        try {
            final ACIInstallationService aciInstallationService = getAciInstallationService();
            return aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid);
        } catch (ConnectApplicationNotFoundException | InstallationNotFoundException e) {
            final String msg = String.format("Cannot retrieve ACI Installation with principalUuid: %s",
                    principalUuid, e.getMessage());
            throw new NotFoundException(msg, e);
        }
    }

    private Installation getAciInstallationByClientKey(final String clientKey) {
        log.debug("Retrieving ACI Installation based on clientKey: {}", clientKey);
        try {
            final ACIInstallationService aciInstallationService = getAciInstallationService();
            return aciInstallationService.getByClientKey(clientKey);
        } catch (ConnectApplicationNotFoundException | InstallationNotFoundException e) {
            final String msg = String.format("Cannot retrieve ACI Installation with clientKey: %s",
                    clientKey, e.getMessage());
            throw new NotFoundException(msg, e);
        }
    }

    private ACIInstallationService getAciInstallationService() {
        return aciInstallationServiceAccessor.get()
                .orElseThrow(() -> new IllegalStateException(
                        "Unable to get an instance of ACIInstallationService from ACIInstallationServiceAccessor"));
    }

    /**
     * @return data required by the view that corresponds to the view name defined in {@link ActionViewData} annotation.
     */
    @ActionViewData(APPROVAL_VIEW_NAME)
    public Map<String, Object> getApprovalViewData() {
        final ImmutableMap.Builder<String, Object> mapBuilder = ImmutableMap.builder();
        mapBuilder.put("baseUrl", salApplicationProperties.getBaseUrl(UrlMode.AUTO));
        mapBuilder.put("orgId", organization.getId());
        mapBuilder.put("grantAccessButtonRedirectUrl", urlProvider.grantAccessButtonRedirectUrl(organization));
        mapBuilder.put("cancelButtonUrl",
                urlProvider.cancelButtonUrlForApprovalView(isJiraInitiated, accountName, installation));
        mapBuilder.put("accountName", installation.getPrincipalUsername());
        mapBuilder.put("jiraInstanceTitle", jiraApplicationProperties.getString(JIRA_TITLE));
        mapBuilder.put("atlToken", getXsrfToken());
        return mapBuilder.build();
    }

    /**
     * @return data required by the view that corresponds to the view name defined in {@link ActionViewData} annotation.
     */
    @ActionViewData(NON_ADMIN_VIEW_NAME)
    public Map<String, Object> getNonAdminViewData() {
        final ImmutableMap.Builder<String, Object> mapBuilder = ImmutableMap.builder();
        mapBuilder.put("baseUrl", salApplicationProperties.getBaseUrl(UrlMode.AUTO));
        mapBuilder.put("okButtonUrl", urlProvider.okButtonForNonAdminView(isJiraInitiated, accountName, installation));
        mapBuilder.put("logoutButtonUrl", urlProvider.logoutButtonUrl());
        mapBuilder.put("accountName", accountName);
        mapBuilder.put("jiraInstanceTitle", jiraApplicationProperties.getString(JIRA_TITLE));
        mapBuilder.put("atlToken", getXsrfToken());
        return mapBuilder.build();
    }

    /**
     * @return data required by the view that corresponds to the view name defined in {@link ActionViewData} annotation.
     */
    @ActionViewData(APPROVAL_ERROR_VIEW_NAME)
    public Map<String, Object> getApprovalErrorViewData() {
        return createErrorViewDataCommon();
    }

    /**
     * @return data required by the view that corresponds to the view name defined in {@link ActionViewData} annotation.
     */
    @ActionViewData(UNKNOWN_ERROR_VIEW_NAME)
    public Map<String, Object> getUnknownErrorViewData() {
        return createErrorViewDataCommon();
    }

    /**
     * @return data required by the view that corresponds to the view name defined in {@link ActionViewData} annotation.
     */
    @ActionViewData(REDIRECT_TO_ADMIN_VIEW_NAME)
    public Map<String, Object> getRedirectToAdminViewData() {
        final ImmutableMap.Builder<String, Object> mapBuilder = ImmutableMap.builder();
        mapBuilder.put("redirectUrl", urlProvider.dvcsConnectorAdminPageUrl());
        return mapBuilder.build();
    }

    private Map<String, Object> createErrorViewDataCommon() {
        final ImmutableMap.Builder<String, Object> mapBuilder = ImmutableMap.builder();
        mapBuilder.put("accountName", this.accountName);
        mapBuilder.put("startAgainButtonUrl", urlProvider
                .startAgainButtonUrlForErrorView(isJiraInitiated, accountName, installation));
        mapBuilder.put("cancelButtonUrl", urlProvider
                .cancelButtonUrlForErrorView(isJiraInitiated, accountName, installation));
        mapBuilder.put("jiraInstanceTitle", jiraApplicationProperties.getString(JIRA_TITLE));
        return mapBuilder.build();
    }
}
