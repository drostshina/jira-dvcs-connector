package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import java.util.Date;


public class QRepositoryPullRequestMapping extends EnhancedRelationalPathBase<QRepositoryPullRequestMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_PULL_REQUEST";
    private static final long serialVersionUID = 1124105570764200298L;

    public final StringPath AUTHOR = createString("AUTHOR");
    public final NumberPath<Integer> COMMENT_COUNT = createInteger("COMMENT_COUNT");
    public final DateTimePath<Date> CREATED_ON = createDateTime("CREATED_ON", Date.class);
    public final StringPath DESTINATION_BRANCH = createString("DESTINATION_BRANCH");
    public final StringPath EXECUTED_BY = createString("EXECUTED_BY");
    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath LAST_STATUS = createString("LAST_STATUS");
    public final StringPath NAME = createString("NAME");
    public final NumberPath<Long> REMOTE_ID = createLong("REMOTE_ID");
    public final StringPath SOURCE_BRANCH = createString("SOURCE_BRANCH");
    public final StringPath SOURCE_REPO = createString("SOURCE_REPO");
    public final NumberPath<Integer> TO_REPOSITORY_ID = createInteger("TO_REPOSITORY_ID");
    public final DateTimePath<Date> UPDATED_ON = createDateTime("UPDATED_ON", Date.class);
    public final StringPath URL = createString("URL");
    public final com.querydsl.sql.PrimaryKey<QRepositoryPullRequestMapping> PULL_REQUEST_PK = createPrimaryKey(ID);

    public QRepositoryPullRequestMapping() {
        super(QRepositoryPullRequestMapping.class, AO_TABLE_NAME);
    }

}