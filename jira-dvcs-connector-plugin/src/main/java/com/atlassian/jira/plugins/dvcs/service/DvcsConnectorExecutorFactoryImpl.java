package com.atlassian.jira.plugins.dvcs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Named;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Named
@ParametersAreNonnullByDefault
public class DvcsConnectorExecutorFactoryImpl implements DvcsConnectorExecutorFactory {

    private static final String LINKER_SERVICE_THREAD_PREFIX = "DVCSConnector.LinkerService";
    private static final String WEBHOOK_CLEANUP_THREAD_PREFIX = "DVCSConnector.WebhookCleanup";
    private static final String REPOSITORY_DELETION_THREAD_PREFIX = "DVCSConnector.RepositoryDeletion";

    private static final long DESTROY_TIMEOUT_SECS = 30;
    private static final Logger LOG = LoggerFactory.getLogger(DvcsConnectorExecutorFactoryImpl.class);

    @Override
    public ThreadPoolExecutor createLinkerServiceThreadPoolExecutor() {
        return createDefaultThreadpoolExecutor(LINKER_SERVICE_THREAD_PREFIX);
    }

    @Override
    public ThreadPoolExecutor createWebhookCleanupThreadPoolExecutor() {
        return createDefaultThreadpoolExecutor(WEBHOOK_CLEANUP_THREAD_PREFIX);
    }

    @Override
    public ThreadPoolExecutor createRepositoryDeletionThreadPoolExecutor() {
        return createSingleThreadExecutor(REPOSITORY_DELETION_THREAD_PREFIX);
    }

    private ThreadPoolExecutor createDefaultThreadpoolExecutor(final String name) {
        return new ThreadPoolExecutor(1, 10, 1, TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(100), namedThreadFactory(name));
    }

    private ThreadPoolExecutor createSingleThreadExecutor(final String name) {
        return new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(), namedThreadFactory(name));
    }

    @Override
    public void shutdownExecutor(final String owningClassName, final ThreadPoolExecutor executor) {
        checkArgument(isNotBlank(owningClassName), "The owning class name must be provided");
        checkNotNull(executor, "An executor must be provided");

        executor.shutdown();
        executor.getQueue().clear();
        try {
            boolean destroyed = executor.awaitTermination(DESTROY_TIMEOUT_SECS, SECONDS);
            if (!destroyed) {
                LOG.error("ExecutorService for {} did not shut down within {}s", owningClassName, DESTROY_TIMEOUT_SECS);
            }
        } catch (InterruptedException e) {
            LOG.error("Interrupted while waiting for ExecutorService to shut down.");
            Thread.currentThread().interrupt();
        }
    }
}
