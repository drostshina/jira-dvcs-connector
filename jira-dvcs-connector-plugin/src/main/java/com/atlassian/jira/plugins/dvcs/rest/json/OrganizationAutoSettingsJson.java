package com.atlassian.jira.plugins.dvcs.rest.json;

/**
 * DTO for updating an organization's <code>autolink</code> and <code>smartcommits</code> properties.
 */
public class OrganizationAutoSettingsJson {
    private boolean enableAutolink;
    private boolean enableSmartCommits;

    public OrganizationAutoSettingsJson() {
    }

    public boolean isEnableAutolink() {
        return enableAutolink;
    }

    public void setEnableAutolink(final boolean enableAutolink) {
        this.enableAutolink = enableAutolink;
    }

    public boolean isEnableSmartCommits() {
        return enableSmartCommits;
    }

    public void setEnableSmartCommits(final boolean enableSmartCommits) {
        this.enableSmartCommits = enableSmartCommits;
    }
}
