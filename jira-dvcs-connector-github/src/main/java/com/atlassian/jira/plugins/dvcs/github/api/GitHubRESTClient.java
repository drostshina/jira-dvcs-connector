package com.atlassian.jira.plugins.dvcs.github.api;

import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRateLimit;
import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRepositoryHook;
import com.atlassian.jira.plugins.dvcs.model.Repository;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

/**
 * API abstraction over GitHub REST API.
 *
 * @author Stanislav Dvorscak
 */
@ParametersAreNonnullByDefault
public interface GitHubRESTClient {

    /**
     * Adds the given hook to the given repository.
     *
     * @param repository on which repository
     * @param hook       for creation
     * @return created hook
     */
    @Nonnull
    GitHubRepositoryHook addHook(Repository repository, GitHubRepositoryHook hook);

    /**
     * Deletes the given hook from the given repository.
     *
     * @param repository on which repository
     * @param hook       for deletion
     */
    void deleteHook(Repository repository, GitHubRepositoryHook hook);

    /**
     * Returns the hooks for the given repository.
     *
     * @param repository for which repository
     * @return returns hooks for provided repository.
     */
    @Nonnull
    List<GitHubRepositoryHook> getHooks(Repository repository);

    /**
     * Returns the hooks for the given repository.
     *
     * @param repository the repository for which to get the hooks
     * @param username the username for HTTP Basic Auth
     * @param password the password for HTTP Basic Auth
     * @return the hooks
     */
    @Nonnull
    List<GitHubRepositoryHook> getHooks(Repository repository, String username, String password);

    /**
     * Returns the rate limit information for the given GitHub account.
     *
     * @param accountName the GitHub account for which to get the rate limit
     * @param password the password for that account
     * @return the rate limit
     */
    @Nonnull
    GitHubRateLimit getRateLimit(String accountName, String password);
}
